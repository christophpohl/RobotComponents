 #!/bin/bash -ex
#

echo "Check if virtualenv is already configured"
echo $(pwd)
# if virtualenv not configured
if [ ! -d "env" ]
	then
		echo "Virtualenv not configured"	
		virtualenv env
		source env/bin/activate
		pip install -r requirements.txt
                cd ../../../../../ArmarXCore/etc/python/
                python setup.py develop
                echo "Installation finished"
                cd -
                python PoseBasedActionRecognizer_parseOption.py -c
	else
		echo "Virtualenv configured"
		source env/bin/activate
		echo "Activated virtualenv"
		echo "Start Script"
		python PoseBasedActionRecognizer_parseOption.py -c
		echo "End Script"
fi
