/**
\page RobotComponents-Tutorials-CollisionChecker Using the CollisionChecker component

\tableofcontents

\section RobotComponents-Tutorials-CollisionChecker-Description Description
The CollisionChecker constantly checks for collisions and calculates distances between SceneObject pairs. The results are reported to the configured topics.
There are two topics provided by the CollisionChecker. The first topic just reports the calculated distances with the configured frequency. The second topic reports whether the distance between two SceneObject pairs falls below a configured value and if the SceneObject pair collides.

\section RobotComponents-Tutorials-CollisionChecker-Configuration Configuration
There are two ways of adding SceneObject pairs to the CollisionChecker. First you can use the \ref armarx::CollisionCheckerInterface::addCollisionPair method. There are also methods to remove a pair, check, if a pair is added and change the distance from which to warn for a collision.
The second way to add SceneObject pairs is to configure them using the "CollisionPairs" parameter. This parameter needs a JSON like array of SceneObjects pairs. Each pair contains two lists of object names and a distance from which to warn. If you configure the CollisionChecker to use the WorkingMemory you need to specify for each list, from which robot the objects should be used. If no robot is given normal objects from the WorkingMemory are used.
\verbatim
[{Armar3:{Ring L J0,Ring L J1},Armar3:{Ring R J0,Ring R J1},20.0},{Armar3:{Ring L J0,Ring L J1},{tablea,tableb,tablec},30.0}]
\endverbatim
To add a RobotNodeSet you can use the following notation:
\verbatim
Armar3:Torso
\endverbatim

\section RobotComponents-Tutorials-CollisionChecker-Debuging Debugging
To debug the CollisionChecker you can enable the DebugDrawer. This will print a colored line for each CollisionPair to a layer of the DebugDrawer. A green line just shows that there is a CollisionPair, a yellow line is displayed, if the distance falls below the configured value.
\image html CollisionCheckerDebugDrawer.png
*/
