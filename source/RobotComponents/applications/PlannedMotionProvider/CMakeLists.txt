armarx_component_set_name("PlannedMotionProviderApp")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    PlannedMotionProvider
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
