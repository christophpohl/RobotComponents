/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::applications::AStarPathPlannerTestApp
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotComponents/interface/components/PathPlanner.h>

namespace armarx
{
    class AStarPathPlannerTestComponentPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        AStarPathPlannerTestComponentPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of WorkingMemory component");
            defineOptionalProperty<std::string>("DebugDrawerName", "DebugDrawerUpdates", "Name of DebugDrawer component");
            defineOptionalProperty<std::string>("AStarPathPlannerName", "AStarPathPlanner", "Name of AStarPathPlanner component");
        }
    };

    /**
     * @brief A test component for the AStarPathPlanner
     */
    class AStarPathPlannerTestComponent:
        virtual public Component
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr
            {
                new AStarPathPlannerTestComponentPropertyDefinitions{getConfigIdentifier()}
            };
        }

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        void onExitComponent() override;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "AStarPathPlannerTestComponent";
        }
    private:
        memoryx::WorkingMemoryInterfacePrx workingMemoryPrx;
        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
        armarx::AStarPathPlannerBasePrx aStarPathPlannerPrx;
    };
}

