armarx_component_set_name("ReflexCombinationApp")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

find_package(IVT COMPONENTS ivt ivtopencv QUIET)
armarx_build_if(IVT_FOUND "ivt library not found")
if(IVT_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
endif()

armarx_build_if(IVT_ivtopencv_FOUND "ivt opencv bridge not available")

set(COMPONENT_LIBS ReflexCombination RobotAPIInterfaces RobotComponentsInterfaces RobotAPIUnits ArmarXCoreInterfaces ArmarXCore RobotAPICore ivt ivtvideocapture ivtopencv ${Simox_LIBRARIES})



set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
