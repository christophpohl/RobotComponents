/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>

module armarx
{
module cprs
{
    /**
     * @brief Base class for all computing power request strategy.
     */
    ["cpp:virtual"]
    class ComputingPowerRequestStrategyBase
    {
        /**
         * @brief Sets the current state as initial state. Should be called after all initial computing power was allocated.
         */
        void setCurrentStateAsInitialState();
        //update data
        /**
         * @brief Updates the planners node count.
         * @param nodeCount The new node count.
         */
        void updateNodeCount(long nodeCount);
        /**
         * @brief Updates the tasks status
         * @param newStatus The new status.
         */
        void updateTaskStatus(TaskStatus::Status newStatus);

        void updateNodeCreations(long nodesCreated, long tries);

        //handel desicions
        /**
         * @brief Called after additional computing power was allocated.
         */
        void allocatedComputingPower();
        /**
         * @brief Returns whether new computing power should be allocated.
         * @return Whether new computing power should be allocated.
         */
        bool shouldAllocateComputingPower();
    };

    sequence<ComputingPowerRequestStrategyBase>  ComputingPowerRequestStrategyBaseList;

    /**
     * @brief Base class for compound strategies.
     *
     * Provides convenience implementations of all update, the setInitialState and allocatedComputingPower methodes.
     */
    ["cpp:virtual"]
    class CompoundedRequestStrategyBase extends ComputingPowerRequestStrategyBase
    {
        /**
         * @brief The compounded strategies
         */
        ComputingPowerRequestStrategyBaseList requestStrategies;
    };

    /**
     * @brief A compound strategy returning true if all compounded strategies return true.
     */
    ["cpp:virtual"]
    class AndBase extends CompoundedRequestStrategyBase{};

    /**
     * @brief A compound strategy returning true if any compounded strategy returns true.
     */
    ["cpp:virtual"]
    class OrBase extends CompoundedRequestStrategyBase{};

    /**
     * @brief A strategy returning true if the contained strategy returns false.
     */
    ["cpp:virtual"]
    class NotBase extends ComputingPowerRequestStrategyBase
    {
        /**
         * @brief The strategy that is inverted.
         */
        ComputingPowerRequestStrategyBase allocStrat;
    };

    /**
     * @brief A strategy always returning true.
     */
    ["cpp:virtual"]
    class AlwaysBase extends ComputingPowerRequestStrategyBase{};

    /**
     * @brief A strategy always returning false.
     */
    ["cpp:virtual"]
    class NeverBase extends ComputingPowerRequestStrategyBase{};

    /**
     * @brief A strategy returning true again after set time delta.
     */
    ["cpp:virtual"]
    class ElapsedTimeBase extends ComputingPowerRequestStrategyBase
    {
        /**
         * @brief The used time delta.
         */
        long timeDeltaInSeconds;
        /**
         * @brief Whether the strategy skips multiple  pending requests.
         * (example: last update at t, now it is t+2d, if true three consecutive calls to shouldAllocateComputingPower()
         * will return true, false, false. if false three calls will return true, true, false)
         */
        bool skipping;
    };

    /**
     * @brief A strategy returning true again after set time delta. T
     * he delta is decreased if there were failed creations of new nodes (it will always be smaller than the set time delta).
     *
     * The used time delta is timeDelta/(1 + sigma * (#failed node creations)/(#tries))
     * The last backlogSize tries will be evaluated.
     */
    ["cpp:virtual"]
    class NoNodeCreatedBase extends ElapsedTimeBase
    {
        /**
         * @brief The used factor to decrease the time delta.
         */
        float sigma;

        long backlogSize;
    };

    /**
     * @brief A strategy returning true again after set node delta.
     */
    ["cpp:virtual"]
    class TotalNodeCountBase extends ComputingPowerRequestStrategyBase
    {
        /**
         * @brief The used node delta.
         */
        long nodeCountDelta;
        /**
         * @brief Whether the strategy skips multiple  pending requests.
         * (example: last update at n, now it is n+2d, if true three consecutive calls to shouldAllocateComputingPower()
         * will return true, false, false. if false three calls will return true, true, false)
         */
        bool skipping;
    };

    dictionary<TaskStatus::Status, ComputingPowerRequestStrategyBase> TaskStatusMap;
    /**
     * @brief A choosing a sub strategy depending on the task status.
     */
    ["cpp:virtual"]
    class TaskStatusBase extends ComputingPowerRequestStrategyBase
    {
        /**
         * @brief The mapping from task status to sub strategies
         */
        TaskStatusMap strategyPerTaskStatus;
    };
};
};
