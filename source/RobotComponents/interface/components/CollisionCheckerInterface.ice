#include <MemoryX/interface/workingmemory/WorkingMemoryListenerInterface.ice>

module armarx
{
    sequence<string> StringList;
    struct CollisionPair
    {
        string robotName1;
        StringList nodeNames1;
        string robotName2;
        StringList nodeNames2;
        double warningDistance;
    };
    sequence<CollisionPair> CollisionPairList;

    interface CollisionCheckerInterface extends memoryx::WorkingMemoryListenerInterface
    {
        void addCollisionPair(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2, double warningDistance);

        void removeCollisionPair(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2);

        ["cpp:const"] idempotent bool hasCollisionPair(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2);

        void setWarningDistance(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2, double warningDistance);
        ["cpp:const"] idempotent double getWarningDistance(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2);

        ["cpp:const"] idempotent CollisionPairList getAllCollisionPairs();

        ["cpp:const"] idempotent int getInterval();
        void setInterval(int interval);
    };
    interface DistanceListener
    {
        void reportDistance(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2, double distance);
    };
    interface CollisionListener
    {
        void reportCollision(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2, double distance);
        void reportCollisionWarning(string robotName1, StringList nodeNames1, string robotName2, StringList nodeNames2, double distance);
    };
};
