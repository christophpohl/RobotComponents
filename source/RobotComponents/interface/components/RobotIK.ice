/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    
 * @author     
 * @date       
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

module armarx
{
    /**
    * Available Cartesian selection options for target poses.
    * See VirtualRobot::IKSolver for more information.
    */
    enum CartesianSelection 
    {
        eX,
        eY,
        eZ,
        ePosition,
        eOrientation,
        eAll
    };

    sequence<string> NodeNameList;

    struct WorkspaceBounds {
        float x;
        float y;
        float z;
        float ro;
        float pi;
        float ya;
    };

    enum InverseJacobiMethod 
    {
        eSVD,
        eSVDDamped,
        eTranspose,
    };

    struct CoMIKDescriptor 
    {
        // REQUIRED:
        int priority; // priority for hierarchical ik
        float gx; // desired center of mass x
        float gy; // desired center of mass y
        string robotNodeSetBodies; // body set for which the center of mass is computed
        float tolerance; // error tolerance
        // OPTIONAL:
        string coordSysName; // coordinate system name in which the center of mass is defined; empty string for global system
    };

    struct DifferentialIKDescriptor
    {
        // REQUIRED:
        int priority;
        string tcpName;
        PoseBase tcpGoal;
        CartesianSelection csel;
        InverseJacobiMethod ijm;
        // OPTIONAL
        string coordSysName;
    };

    struct ExtendedIKResult
    {
        bool isReachable;
        float errorPosition;
        float errorOrientation;
        NameValueMap jointAngles;
    };

    sequence<DifferentialIKDescriptor> IKTasks;

    interface RobotIKInterface
    {
        

        // TODO: throw exceptions
        /**
         * @brief Computes a single IK solution for the given robot node set and desired TCP pose.
         * @details The TCP pose can be defined in any frame and is converted internally to a global pose 
         * according to the current robot state. The CartesianSelection
         * parameter defines which part of the target pose should be reached.
         * 
         * @param robotNodeSetName The name of a robot node set (robot node set) that is either stored 
         * within the robot model or has been defined via \ref defineRobotNodeSet.
         * @param tcpPose The framed target pose for the TCP.
         * @param cs Specifies which part of the pose needs to be reached by the IK, e.g. the position only,
         *           the orientation only or all.
         * @return A map that maps each joint name to its value in the found IK solution. 
         */
        NameValueMap computeIKFramedPose(string robotNodeSetName,
                                                        FramedPoseBase tcpPose,
                                                        CartesianSelection cs);
        
        /**
         * @brief Computes a single IK solution for the given robot node set and desired global TCP pose.
         * @details The TCP pose is assumed to be in the global frame. The CartesianSelection
         * parameter defines which part of the target pose should be reached.
         * 
         * @param robotNodeSetName The name of a robot node set (robot node set) that is either stored 
         * within the robot model or has been defined via \ref defineRobotNodeSet.
         * @param tcpPose The global target pose for the TCP.
         * @param cs Specifies which part of the pose needs to be reached by the IK, e.g. the position only,
         *           the orientation only or all.
         * @return A map that maps each joint name to its value in the found IK solution. 
         */
        NameValueMap computeIKGlobalPose(string robotNodeSetName,
                                                        PoseBase tcpPose,
                                                        CartesianSelection cs);

        /**
         * @brief Computes a single IK solution, error and reachability for the given robot node set and desired global TCP pose.
         * @details The TCP pose is assumed to be in the global frame. The CartesianSelection
         * parameter defines which part of the target pose should be reached.
         *
         * @param robotNodeSetName The name of a robot node set (robot node set) that is either stored
         * within the robot model or has been defined via \ref defineRobotNodeSet.
         * @param tcpPose The global target pose for the TCP.
         * @param cs Specifies which part of the pose needs to be reached by the IK, e.g. the position only,
         *           the orientation only or all.
         * @return A map that maps each joint name to its value in the found IK solution, the reachability and computational error.
         */
        ExtendedIKResult computeExtendedIKGlobalPose(string robotNodeSetName,
                                                        PoseBase tcpPose,
                                                        CartesianSelection cs);

        /**
         * @brief Computes an IK solution for the given robot joints such that the center of mass lies above the 
         * given point.
         * @details 
         * 
         * @param robotNodeSetJoints Name of the robot node set that contains the joints you wish to compute the IK for.
         * @param comIK A center of mass description. Note that the priority field is relevant for this function as there
         * is only a single CoM of descriptor.
         * 
         * @return The ik-solution. Returns an empty vector if there is no solution.
         */
        NameValueMap computeCoMIK(string robotNodeSetJoints,
                                                        CoMIKDescriptor comIK);

        /**
         * @brief Computes a configuration gradient in order to solve several tasks/constraints simultaneously.
         * @details This function allows you to use the HierarchicalIK solver provided by Simox. 
         * It computes a configuration gradient for the given robot node set that minimizes the workspace errors
         * for multiple tasks simultaneously. You can specify IK tasks, a center of mass task and a joint limit avoidance task.
         * For details for the different type of tasks, see the parameter description. You must define a priority for each task.
         * The task with maximal priority is the first task to be solved. Each subsequent task is then solved 
         * within the null space of the previous task. <b> Note that this function returns a gradient and NOT
         * an absolute IK solution. The gradient is computed on the current configuration of the robot. <\b>.
         * The gradient is computed from the current robot configuration.
         *
         * See @url http://simox.sourceforge.net/documentation/class_virtual_robot_1_1_hierarchical_i_k.html 
         * and @url http://sourceforge.net/p/simox/wiki/VirtualRobot/#hierarchical-ik-solving for more information.
         * 
         * @param robotNodeSet The robot node set (e.g. kinematic tree) you wish to compute the gradient for.
         * @param diffIKs A list of IK tasks. Each IK task specifies a TCP and a desired pose for this TCP.
         * @param comIK A center of mass tasks. Defines where the center should be and its priority. Is only used if the 
                        CoM-task is enabled.
         * @param stepSize
         * @param avoidJointLimits Set whether or not to avoid joint limits.
         * @param enableCenterOfMass Set whether or not to adjust the center of mass.
         * @return A configuration gradient...
         */
        NameValueMap computeHierarchicalDeltaIK(string robotNodeSet,
                                                            IKTasks diffIKs,
                                                            CoMIKDescriptor comIKs,
                                                            float stepSize,
                                                            bool avoidJointLimits,
                                                            bool enableCenterOfMass);
        /**
         * @brief Defines a new robot node set.
         * @details Define a new robot node set with the given name that consists out of the given list of nodes with given 
         *          TCP and root frame. Iff the chain is successfully added or already exists, <it>true</it> is returned.
         * @param name The name of the robot node set.
         * @param nodes The list of robot nodes that make up the robot node set.
         * @param tcpName The name of the TCP node.
         * @param rootNode The name of the kinematic root.
         * 
         * @return True, iff chain was added or already exists. False, iff a different chain with similar name already exists.
         */
        bool defineRobotNodeSet(string name, NodeNameList nodes, string tcpName, string rootNode);

        /**
         * @brief Creates a new reachability space for the given robot node set.
         * @details If there is no reachability space for the given robot node set yet, a new one is created. This may take 
         *          some time. The function returns true iff a reachability space for the given robot node set exists after 
         *          execution of this function.
         * 
         * @param chainName The name of a defined robot node set. This can be either a robot node set that is defined in the 
         *                  robot model or a robot node set that has been manually defined calling \ref defineRobotNodeSet.
         * @param coordinateSystem The name of the robot node in whose coordinate system the reachability space shall be defined in. If you wish to choose the global coordinate system, pass an empty string. Note, however, that any reachability space defined in the global coordinate system gets invalidated once the robot moves.
         * @param stepTranslation The extend of a voxel dimension in translational dimensions (x,y,z) [mm]
         * @param stepRotation The extend of a voxel dimension in rotational dimensions (roll,pitch,yaw) [rad]
         * @param minBounds The minimum workspace poses (x,y,z,ro,pi,ya) given in the base node's coordinate system [mm and rad]
         * @param maxBounds The maximum workspace poses (x,y,z,ro,pi,ya) given in base node's coordinate system [mm and rad]
         * @param numSamples The number of tcp samples to take to create the reachability space (e.g 1000000)
         * @return True iff the a reachability space for the given robot node set is available after execution of this function.
         *         False in case of a failure, e.g. there is no chain with the given name.
         */
        bool createReachabilitySpace(string chainName, string coordinateSystem, float stepTranslation, float stepRotation,
                                     WorkspaceBounds minBounds, WorkspaceBounds maxBounds, int numSamples);

        /**
         * @brief Returns whether this component has a reachability space for the given robot node set.
         * @details True if there is a reachability space available, else false.
         * 
         * @param chainName Name of the robot node set.
         * @return True if there is a reachability space available, else false.
         */
        ["cpp:const"] idempotent bool hasReachabilitySpace(string chainName);

        /**
         * @brief Loads the reachability space from the given file.
         * @details If loading the reachability space succeeds, the reachability space is ready to be used after this function 
         *          terminates. A reachability space can only be loaded if there is no reachability space for the respective 
         *          robot node set yet.
         * 
         * @param filename Binary file containing a reachability space. Ensure that the path is valid and accessible from
         *                 where this component is running.
         * @return True iff loading the reachability space was successful.
         */
        bool loadReachabilitySpace(string filename);

        /**
         * @brief Saves a previously created reachability space of the given robot node set.
         * @details A reachability space for a robot node set can only be saved, if actually is one.
         *          You can check whether a reachability space is available by calling \ref hasReachabilitySpace(robotNodeSet).
         * 
         * @param robotNodeSet The robot node set for which you wish to save the reachability space.
         * @param filename The filename if the file(must be an accessible path for this component) you wish to save the space in.
         * @return True iff saving was successful.
         */
        ["cpp:const"] idempotent bool saveReachabilitySpace(string robotNodeSet, string filename);

        /**
         * @brief Returns whether a given framed pose is currently reachable by the TCP of the given robot node set.
         * @details To determine whether a pose is reachable a reachability space for the given robot node set is 
         *          required. If no such space exists, the function returns <it>false<\it>. 
         *          Call \ref createReachabilitySpace first to ensure there is such a space.
         * 
         * @param chainName A name of a robot node set either defined in the robot model or previously defined via 
         *                  \ref defineRobotNodeSet. 
         * @param framedPose A framed pose to check for reachability. The pose is transformed into a global pose using the 
         *                   current robot state.
         * 
         * @return True, if the pose is reachable by the TCP of the given chain. False, if it is not reachable or 
         *         there is no reachability space for the given chain available.
         */
        ["cpp:const"] idempotent bool isFramedPoseReachable(string chainName, FramedPoseBase framedPose);

        /**
         * @brief Returns whether a given global pose is currently reachable by the TCP of the given robot node set.
         * @details To determine whether a pose is reachable a reachability space for the given robot node set is 
         *          required. If no such space exists, the function returns <it>false<\it>. 
         *          Call \ref createReachabilitySpace first to ensure there is such a space.
         * 
         * @param chainName A name of a robot node set either defined in the robot model or previously defined via 
         *                  \ref defineRobotNodeSet. 
         * @param pose A global pose to check for reachability. 
         * 
         * @return True, if the pose is reachable by the TCP of the given chain. False, if it is not reachable or 
         *         there is no reachability space for the given chain available.
         */
        ["cpp:const"] idempotent bool isPoseReachable(string chainName, PoseBase framedPose);


    };
};
