/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Patrick Grube
* @copyright  2015
* @license    http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

//#include <RobotComponents/interface/components/TrajectoryPlayerInterface.ice>

module armarx
{
    interface MMMPlayerInterface
    {
        bool loadMMMFile(string filename, string projects);
        int getNumberOfFrames();
        string getMotionPath();
        string getModelPath();
        Ice::StringSeq getJointNames();

//        TrajSource getTrajectory();

        TrajectoryBase getJointTraj();
        TrajectoryBase getBasePoseTraj();

        bool isMotionLoaded();

    };
};

