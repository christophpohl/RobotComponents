/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotTrajectoryDesigner::gui-plugins::RobotTrajectoryDesignerGuiPlugin
 * \author     Timo Birr ( timo dot birr at t-online dot de )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_RobotTrajectoryDesigner_RobotTrajectoryDesignerGuiPlugin_GuiPlugin_H
#define _ARMARX_RobotTrajectoryDesigner_RobotTrajectoryDesignerGuiPlugin_GuiPlugin_H

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{
    /**
     * \class RobotTrajectoryDesignerGuiPluginGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief RobotTrajectoryDesignerGuiPluginGuiPlugin brief description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotTrajectoryDesignerGuiPluginGuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        RobotTrajectoryDesignerGuiPluginGuiPlugin();
    };
}

#endif
