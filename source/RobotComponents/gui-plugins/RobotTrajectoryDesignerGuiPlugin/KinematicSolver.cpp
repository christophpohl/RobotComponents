/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KinematicSolver.h"
#include "VirtualRobot/Workspace/Reachability.h"
#include "MotionPlanning/CSpace/CSpaceSampled.h"
#include "MotionPlanning/Planner/BiRrt.h"
#include "MotionPlanning/Planner/Rrt.h"
#include "VirtualRobot/CollisionDetection/CDManager.h"
#include "MotionPlanning/PostProcessing/ShortcutProcessor.h"
#include "RobotAPI/libraries/core/math/MathUtils.h"
#include "RobotComponents/components/RobotIK/RobotIK.h"
#include "Interpolation/LinearInterpolation.h"


using namespace armarx;
using namespace VirtualRobot;

KinematicSolverPtr KinematicSolver::singleton = 0;

KinematicSolver::KinematicSolver(ScenePtr scenario, RobotPtr robot)
{
    //bla
    //initialize attributes
    this->robot = robot;
    //this->scenario = scenario;
    this->robotProxy = robot->clone();
    this->currentRns = "";

    //initialize IKCalibrations
    this->IKCalibrationMap = std::map<std::string, IKCalibration>();
    this->IKCalibrationMap.insert(std::pair<std::string, IKCalibration>("Armar3", IKCalibration::createIKCalibration(1000, 0.005, 500, 0.3, 100, 1.0)));
    this->IKCalibrationMap.insert(std::pair<std::string, IKCalibration>("Armar4", IKCalibration::createIKCalibration(10000, 0.005, 100, 0.6, 100, 1.0)));
    this->IKCalibrationMap.insert(std::pair<std::string, IKCalibration>("Armar6", IKCalibration::createIKCalibration(100, 0.5, 500, 1.1, 500, 1.1)));
    this->IKCalibrationMap.insert(std::pair<std::string, IKCalibration>("Custom", IKCalibration::createIKCalibration(100, 100, 100, 100, 100, 100)));

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///ALTERNATIVE WITH REACHABILITY MAPS
    //this->scenario = scenario;
    /*this->reachabilityMap = std::map<std::string, ReachabilityPtr>();
    for (RobotNodeSetPtr rns : robotProxy->getRobotNodeSets())
    {
        if (!rns->isKinematicChain())
        {
            continue;
        }
        ReachabilityPtr currentReach = ReachabilityPtr(new Reachability(robotProxy));
        float minB[6] = { -1000.0f, -1000.0f, -1000.0f, (float) - M_PI, (float) - M_PI, (float) - M_PI};
        float maxB[6] = {1000.0f, 1000.0f, 1000.0f, (float)M_PI, (float)M_PI, (float)M_PI};
        ARMARX_INFO << robotProxy->getRootNode()->getName();
        currentReach->initialize(rns, 200.0, 0.4, minB, maxB, rns, rns, robotProxy->getRootNode(), rns->getTCP(), true);
        currentReach->addRandomTCPPoses(100, 2, false);
        reachabilityMap[rns->getName()] = currentReach;
    }*/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

KinematicSolver::IKCalibration KinematicSolver::IKCalibration::createIKCalibration(int smallIterations, double smallStepSize, int mediumIterations, double mediumStepSize, int largeIterations, double largeStepSize)
{
    IKCalibration calibration;
    calibration.smallIterations = smallIterations;
    calibration.smallStepSize = smallStepSize;
    calibration.mediumIterations = mediumIterations;
    calibration.mediumStepSize = mediumStepSize;
    calibration.largeIterations = largeIterations;
    calibration.largeStepSize = largeStepSize;
    return calibration;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///SINGLETON-FEATURES////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
KinematicSolverPtr KinematicSolver::getInstance(ScenePtr scenario = NULL, RobotPtr robot = NULL)
{
    if (singleton == 0)
    {
        ARMARX_INFO << "KinematicSolver initialized";
        singleton = KinematicSolverPtr(new KinematicSolver(scenario, robot));
        return singleton;
    }
    return KinematicSolver::singleton;
}

void KinematicSolver::reset()
{
    ARMARX_INFO << "KinematicSolver reset";
    singleton = 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///IK////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


std::vector<double> KinematicSolver::solveIK(RobotNodeSetPtr kc, PoseBasePtr globalPose, IKSolver::CartesianSelection selection, int iterations)
{
    IKCalibration calibration = calibrate();
    syncRobotPrx(kc);
    //get Proxy for Rns
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());
    std::vector<double> jointValues;
    int failureCounter = 0;

    do
    {
        //set all joints randomly so a random solution for IK Problem is calculated
        std::vector<float> jv;
        float rn = 1.0f / (float)RAND_MAX;
        for (unsigned int i = 0; i < kcPrx->getSize(); i++)
        {
            RobotNodePtr ro =  kcPrx->getNode(i);
            float r = (float)rand() * rn;
            float v = ro->getJointLimitLo() + (ro->getJointLimitHi() - ro->getJointLimitLo()) * r;
            jv.push_back(v);
        }
        robotProxy->setJointValues(kcPrx, jv);
        //finished randomly setting starting Pose

        //solve actual IK
        GenericIKSolverPtr ikSolver = this->IKSetup(calibration.largeStepSize, calibration.largeIterations, 10.0, 0.50, kcPrx);
        Pose targetPose = Pose(globalPose->position, globalPose->orientation);
        std::vector<float> output = ikSolver->solveNoRNSUpdate(targetPose.toEigen(), selection);
        jointValues = std::vector<double>(output.begin(), output.end());
        failureCounter++;
    }
    while (jointValues.size() == 0 && failureCounter <= iterations);    //randomly resets the robot Pose
    return jointValues;
}

std::vector<double> KinematicSolver::solveIKRelative(RobotNodeSetPtr kc, PoseBasePtr framedPose, IKSolver::CartesianSelection selection)
{
    PosePtr localPose = PosePtr(new Pose(framedPose->position, framedPose->orientation));
    PoseBasePtr globalPose = PoseBasePtr(new Pose(kc->getKinematicRoot()->toGlobalCoordinateSystem(localPose->toEigen())));
    return this->solveIK(kc, globalPose, selection, 50);
}


std::vector<float> KinematicSolver::solveRecursiveIK(RobotNodeSetPtr kc, std::vector<float> startingPoint, PoseBasePtr globalDestination, VirtualRobot::IKSolver::CartesianSelection selection)
{
    IKCalibration calibration = calibrate();
    syncRobotPrx(kc);
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());
    kcPrx->setJointValues(startingPoint);
    GenericIKSolverPtr ikSolver = this->IKSetup(calibration.mediumStepSize, calibration.mediumIterations, 10.0, 1.0, kcPrx);
    Pose targetPose = Pose(globalDestination->position, globalDestination->orientation);
    std::vector<float> output = ikSolver->solveNoRNSUpdate(targetPose.toEigen(), selection);
    if (output.size() == 0)
    {
        std::vector<double> random = this->solveIK(kc, PoseBasePtr(new Pose(targetPose.toEigen())), selection, 5);
        output = std::vector<float>(random.begin(), random.end());
    }
    return output;
}

std::vector<float> KinematicSolver::solveRecursiveIKRelative(RobotNodeSetPtr kc, std::vector<float> startingPoint, PoseBasePtr framedDestination, IKSolver::CartesianSelection selection)
{
    syncRobotPrx(kc);
    PosePtr localPose = PosePtr(new Pose(framedDestination->position, framedDestination->orientation));
    //std::cout << localPose->toEigen();
    PoseBasePtr globalPose = PoseBasePtr(new Pose(kc->getKinematicRoot()->toGlobalCoordinateSystem(localPose->toEigen())));
    return this->solveRecursiveIK(kc, startingPoint, globalPose, selection);
}

std::vector<double> KinematicSolver::solveRecursiveIK(RobotNodeSetPtr kc, std::vector<double> startingPoint, PoseBasePtr globalDestination, IKSolver::CartesianSelection selection)
{
    IKCalibration calibration = calibrate();
    syncRobotPrx(kc);
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());
    kcPrx->setJointValues(std::vector<float>(startingPoint.begin(), startingPoint.end()));
    GenericIKSolverPtr ikSolver = this->IKSetup(calibration.smallStepSize, calibration.smallIterations, 10, 0.5, kcPrx);
    Pose targetPose = Pose(globalDestination->position, globalDestination->orientation);
    //std::cout << targetPose.toEigen();
    std::vector<float> output = ikSolver->solveNoRNSUpdate(targetPose.toEigen(), selection);
    std::vector<double> outputd = std::vector<double>(output.begin(), output.end());
    return outputd;
}

std::vector<double> KinematicSolver::solveRecursiveIKRelative(RobotNodeSetPtr kc, std::vector<double> startingPoint, PoseBasePtr framedDestination, IKSolver::CartesianSelection selection)
{
    syncRobotPrx(kc);
    PosePtr localPose = PosePtr(new Pose(framedDestination->position, framedDestination->orientation));
    PoseBasePtr globalPose = PoseBasePtr(new Pose(kc->getKinematicRoot()->toGlobalCoordinateSystem(localPose->toEigen())));
    return this->solveRecursiveIK(kc, startingPoint, globalPose, selection);
}

std::vector<std::vector<double>> KinematicSolver::solveRecursiveIK(RobotNodeSetPtr kc, std::vector<double> startingPoint, std::vector<PoseBasePtr> globalDestinations, std::vector<IKSolver::CartesianSelection> selections)
{
    if (!isReachable(kc, globalDestinations[globalDestinations.size() - 1], selections[selections.size() - 1]))
    {
        throw LocalException("Desired Pose is not Reachable");
    }
    syncRobotPrx(kc);
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());

    std::vector<std::vector<double>> bestResult;
    int minSingularities = -1;
    int tries = 0;
    while ((minSingularities == -1 || minSingularities != 0) && tries < 2)
    {
        int i = 0;
        //setup data
        int failureCounter = 0;
        int singularities = 0;
        std::vector<std::vector<double>> result = std::vector<std::vector<double>>();
        std::vector<float> currentStart = std::vector<float>(startingPoint.begin(), startingPoint.end());
        //solve ik step-by-step
        for (PoseBasePtr currentDest : globalDestinations)
        {
            std::vector<double> temp = std::vector<double>(currentStart.begin(), currentStart.end());
            if (i != 0)
            {
                PosePtr priorPose = PosePtr(new Pose(globalDestinations.at(i - 1)->position, globalDestinations.at(i - 1)->orientation));
                PosePtr currentPose = PosePtr(new Pose(currentDest->position, currentDest->orientation));
                if (priorPose->toEigen() == currentPose->toEigen() && temp.size() != 0)
                {

                    i++;
                    continue;
                }
            }
            std::vector<double> currentResult = KinematicSolver::getInstance()->solveRecursiveIK(kc, temp, currentDest, selections.at(i));
            if (currentResult.size() == 0)
            {
                failureCounter++;
                if (failureCounter >= 1)
                {
                    failureCounter = 0;
                    singularities++;
                    Eigen::VectorXf start = Eigen::VectorXf(temp.size());
                    Eigen::VectorXf goal = Eigen::VectorXf(temp.size());
                    std::vector<double> end = this->solveIK(kc, currentDest, selections[i], 100);
                    if (end.size() == 0)
                    {
                        ARMARX_INFO << "No local result found";
                        i++;
                        failureCounter++;
                        continue;
                    }
                    int k = 0;
                    for (double d : end)
                    {
                        goal[k] = d;
                        k++;
                    }
                    k = 0;
                    for (double d : temp)
                    {
                        start[k] = d;
                        k++;
                    }
                    std::vector<double> nowStart = std::vector<double>();
                    IKSolver::CartesianSelection sel = selections[i];
                    nowStart = std::vector<double>(currentStart.begin(), currentStart.end());
                    int bestNode = getFurthestNode(kcPrx, start, goal);
                    RobotNodePtr node = kcPrx->getAllRobotNodes()[bestNode];
                    //ARMARX_WARNING << node->getName();
                    kcPrx->setJointValues(currentStart);

                    PoseBasePtr startPose = PoseBasePtr(new Pose(node->getGlobalPose()));
                    kcPrx->setJointValues(goal);
                    PoseBasePtr endPose = PoseBasePtr(new Pose(node->getGlobalPose()));
                    std::vector<PoseBasePtr> cp = {startPose, endPose};
                    IKCalibration calibration = calibrate();
                    AbstractInterpolationPtr ip = AbstractInterpolationPtr(new LinearInterpolation(cp));
                    for (int i = 0; i < 25; i++)
                    {
                        PoseBasePtr poseBase = ip->getPoseAt(i * 0.04);
                        PosePtr posePose = PosePtr(new Pose(poseBase->position, poseBase->orientation));
                        GenericIKSolverPtr ikSolver = this->IKSetup(calibration.smallStepSize, calibration.smallIterations, 5, 0.5, kcPrx);
                        DifferentialIKPtr dIK = ikSolver->getDifferentialIK();
                        dIK->initialize();
                        PosePtr pose = PosePtr(new Pose(currentDest->position, currentDest->orientation));

                        dIK->setGoal(posePose->toEigen(), node, IKSolver::CartesianSelection::X);
                        dIK->setGoal(pose->toEigen(), kcPrx->getTCP(), sel);
                        kcPrx->setJointValues(std::vector<float>(nowStart.begin(), nowStart.end()));

                        if (dIK->solveIK(0.4))
                        {
                            //ARMARX_INFO << "IK SOLUTION FOUND " + std::to_string(i);
                            std::vector<float> ca = kcPrx->getJointValues();
                            nowStart = std::vector<double>(ca.begin(), ca.end());
                            result.push_back(std::vector<double>(ca.begin(), ca.end()));
                        }
                        else
                        {
                            dIK->setGoal(posePose->toEigen(), node, IKSolver::CartesianSelection::Y);
                            kcPrx->setJointValues(std::vector<float>(nowStart.begin(), nowStart.end()));
                            if (dIK->solveIK(0.4))
                            {
                                //ARMARX_INFO << "IK SOLUTION FOUND " + std::to_string(i);
                                std::vector<float> ca = kcPrx->getJointValues();
                                nowStart = std::vector<double>(ca.begin(), ca.end());
                                result.push_back(std::vector<double>(ca.begin(), ca.end()));
                            }
                            else
                            {
                                if (dIK->solveIK(0.4))
                                {
                                    dIK->setGoal(posePose->toEigen(), node, IKSolver::CartesianSelection::Z);
                                    kcPrx->setJointValues(std::vector<float>(nowStart.begin(), nowStart.end()));
                                    //ARMARX_INFO << "IK SOLUTION FOUND " + std::to_string(i);
                                    std::vector<float> ca = kcPrx->getJointValues();
                                    nowStart = std::vector<double>(ca.begin(), ca.end());
                                    result.push_back(std::vector<double>(ca.begin(), ca.end()));
                                }
                                else
                                {
                                    // ARMARX_INFO << "NO IK SOLUTION FOUND " + std::to_string(i);
                                }
                            }
                            if (singularities == 1)
                            {
                                //ARMARX_INFO << "NO IK SOLUTION FOUND " + std::to_string(i);
                            }
                        }

                        /*for (unsigned int j = 0; j < result[0].size(); j++)
                        {
                            if (!kcPrx->getAllRobotNodes()[j]->isLimitless() && abs(result[result.size() - 2][j] - result[result.size() - 1][j]) > 0.1 && i > 0)
                            {
                                ARMARX_INFO << "WRONG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
                                ARMARX_INFO << result[result.size() - 2];
                                ARMARX_INFO << result[result.size() - 1];
                            }
                            else if (kcPrx->getAllRobotNodes()[j]->isLimitless() && abs(math::MathUtils::AngleDelta(result[result.size() - 2][j], result[result.size() - 1][j])) > 0.1 && i > 0)
                            {
                                ARMARX_INFO << "WRONG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
                                ARMARX_INFO << result[result.size() - 2];
                                ARMARX_INFO << result[result.size() - 1];
                            }
                        }*/

                    }
                    currentStart = std::vector<float>(nowStart.begin(), nowStart.end());
                }

            }

            else
            {
                result.push_back(currentResult);
                currentStart = std::vector<float>(currentResult.begin(), currentResult.end());
            }

        }
        tries++;
        if (minSingularities <= 0 || singularities <= minSingularities)
        {
            minSingularities = singularities;
            bestResult = result;
        }
    }
    return bestResult;
}



std::vector<std::vector<double>> KinematicSolver::solveRecursiveIKNoMotionPlanning(RobotNodeSetPtr kc, std::vector<double> startingPoint, std::vector<PoseBasePtr> globalDestinations, std::vector<IKSolver::CartesianSelection> selections)
{
    syncRobotPrx(kc);
    //setup data
    int failureCounter = 0;
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());
    std::vector<std::vector<double>> result = std::vector<std::vector<double>>();
    std::vector<float> currentStart = std::vector<float>(startingPoint.begin(), startingPoint.end());
    int i = 0;

    //solve ik step-by-step
    for (PoseBasePtr currentDest : globalDestinations)
    {
        std::vector<double> temp = std::vector<double>(currentStart.begin(), currentStart.end());
        if (i != 0)
        {
            PosePtr priorPose = PosePtr(new Pose(globalDestinations.at(i - 1)->position, globalDestinations.at(i - 1)->orientation));
            PosePtr currentPose = PosePtr(new Pose(currentDest->position, currentDest->orientation));
            if (priorPose->toEigen() == currentPose->toEigen() && temp.size() != 0)
            {
                result.push_back(temp);
                i++;
                continue;
            }
        }
        std::vector<double> currentResult = KinematicSolver::getInstance()->solveRecursiveIK(kc, temp, currentDest, selections.at(i));
        i++;
        if (currentResult.size() == 0)
        {
            result.push_back(temp);
            failureCounter++;
            currentStart = std::vector<float>(temp.begin(), temp.end());
            if (failureCounter > (double)globalDestinations.size() / 20.0)
            {
                throw LocalException("Too, many faults at current calculation: " + std::to_string(failureCounter) + " Poses not reachable - only " + std::to_string((double)globalDestinations.size() / 5.0) + " allowed!");
            }

        }
        else
        {
            //std::cout << "OK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
            result.push_back(currentResult);
            currentStart = std::vector<float>(currentResult.begin(), currentResult.end());
        }
    }
    return result;
}




std::vector<std::vector<double>> KinematicSolver::solveRecursiveIK(VirtualRobot::RobotNodeSetPtr kc, std::vector<double> startingPoint, std::vector<PoseBasePtr> globalDestinations, IKSolver::CartesianSelection selection)
{
    syncRobotPrx(kc);
    std::vector<IKSolver::CartesianSelection> selections = std::vector<IKSolver::CartesianSelection>();
    for (unsigned int i = 0; i < globalDestinations.size(); i++)
    {
        selections.push_back(selection);
    }
    return this->solveRecursiveIK(kc, startingPoint, globalDestinations, selections);
}

std::vector<std::vector<double> > KinematicSolver::solveRecursiveIKRelative(RobotNodeSetPtr kc, std::vector<double> startingPoint, std::vector<PoseBasePtr> framedDestinations, std::vector<IKSolver::CartesianSelection> selections)
{
    syncRobotPrx(kc);
    std::vector<PoseBasePtr> globalDestinations = std::vector<PoseBasePtr>();
    for (PoseBasePtr framedDestination : framedDestinations)
    {
        PosePtr localPose = PosePtr(new Pose(framedDestination->position, framedDestination->orientation));
        //std::cout << localPose->toEigen();
        PoseBasePtr globalPose = PoseBasePtr(new Pose(kc->getKinematicRoot()->toGlobalCoordinateSystem(localPose->toEigen())));
        globalDestinations.push_back(globalPose);
    }
    return this->solveRecursiveIK(kc, startingPoint, globalDestinations, selections);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///FORWARD KINEMATIC & REACHABILITY///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PoseBasePtr KinematicSolver::doForwardKinematic(RobotNodeSetPtr kc, std::vector<double> jointAngles)
{
    syncRobotPrx(kc);
    RobotNodeSetPtr kcPrx = this->setupProxy(kc, jointAngles);
    return PoseBasePtr(new Pose(kcPrx->getTCP()->getGlobalPose()));
}

PoseBasePtr KinematicSolver::doForwardKinematicRelative(RobotNodeSetPtr kc, std::vector<double> jointAngles)
{
    syncRobotPrx(kc);
    RobotNodeSetPtr kcPrx = this->setupProxy(kc, jointAngles);
    return PoseBasePtr(new Pose(kcPrx->getKinematicRoot()->toLocalCoordinateSystem(kcPrx->getTCP()->getGlobalPose())));
}


bool KinematicSolver::isReachable(RobotNodeSetPtr rns, PoseBasePtr pose, IKSolver::CartesianSelection cs)
{
    syncRobotPrx(rns);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///ALTERNATIVE WITH REACHABILITY MAPS
    /*RobotNodeSetPtr set = robotProxy->getRobotNodeSet(rns->getName());
    if ((reachabilityMap.find(set->getName()) == reachabilityMap.end()) && set->isKinematicChain())
    {
        ReachabilityPtr currentReach = ReachabilityPtr(new Reachability(robotProxy));
        float minB[6] = { -1000.0f, -1000.0f, -1000.0f, (float) - M_PI, (float) - M_PI, (float) - M_PI};
        float maxB[6] = {1000.0f, 1000.0f, 1000.0f, (float)M_PI, (float)M_PI, (float)M_PI};
        ARMARX_INFO << robotProxy->getRootNode()->getName();
        currentReach->initialize(rns, 200.0, 0.4, minB, maxB, set, set, robotProxy->getRootNode(), set->getTCP(), true);
        currentReach->addRandomTCPPoses(1000000, 2, false);
        reachabilityMap[set->getName()] = currentReach;
    }
    ReachabilityPtr reach = reachabilityMap[rns->getName()];
    return reach->isReachable(Pose(pose->position, pose->orientation).toEigen());*/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    return this->solveIK(rns, pose, cs, 50).size() != 0;
}

void KinematicSolver::syncRobotPrx(RobotNodeSetPtr rns)
{
    if (currentRns != rns->getName())
    {
        currentRns = rns->getName();
        this->syncRobotPrx();
    }
}

void KinematicSolver::syncRobotPrx()
{
    ARMARX_INFO << "Syncing Proxy";
    for (RobotNodePtr node : robot->getRobotNodes())
    {
        this->robotProxy->getRobotNode(node->getName())->setJointValue(node->getJointValue());
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///CONVINIENCE-METHODS///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GenericIKSolverPtr KinematicSolver::IKSetup(double jacStepSize, int IKIterations, double vectorError, double orientationError, RobotNodeSetPtr kc)
{
    GenericIKSolverPtr ikSolver = GenericIKSolverPtr(new GenericIKSolver(kc, JacobiProvider::eSVDDamped));
    ikSolver->setupJacobian(jacStepSize, IKIterations);
    ikSolver->setMaximumError(vectorError, orientationError);
    return ikSolver;
}

RobotNodeSetPtr  KinematicSolver::setupProxy(RobotNodeSetPtr kc, std::vector<double> jointAngles)
{
    RobotNodeSetPtr kcPrx = robotProxy->getRobotNodeSet(kc->getName());
    std::vector<float> jointValues = std::vector<float>(jointAngles.begin(), jointAngles.end());
    kcPrx->setJointValues(jointValues);
    return kcPrx;
}

KinematicSolver::IKCalibration KinematicSolver::calibrate()
{

    if (robot->getName() != "Armar3" && robot->getName() != "Armar4" && robot->getName() != "Armar6")
    {
        return this->IKCalibrationMap.at("Armar3");
    }
    else
    {
        return this->IKCalibrationMap.at(robot->getName());
    }
}

double KinematicSolver::getMaxDistance(Eigen::Vector3f destination, std::vector<std::vector<double> > jointAngles, RobotNodeSetPtr rns)
{
    double max = 0;
    for (std::vector<double> ja : jointAngles)
    {
        PoseBasePtr pose = doForwardKinematicRelative(rns, ja);
        double distance = sqrt(pow(pose->position->x - destination[0], 2) + pow(pose->position->y - destination[1], 2) + pow(pose->position->z - destination[2], 2));
        if (distance > max)
        {
            max = distance;
        }
    }
    return max;
}

int KinematicSolver::getFurthestNode(RobotNodeSetPtr rns, Eigen::VectorXf startConfig, Eigen::VectorXf endConfig)
{
    double max = 0;
    int best = rns->getSize() - 1;
    rns->setJointValues(startConfig);
    std::vector<Eigen::Vector3f> startPoses = std::vector<Eigen::Vector3f>();
    std::vector<Eigen::Vector3f> endPoses = std::vector<Eigen::Vector3f>();
    for (RobotNodePtr node : rns->getAllRobotNodes())
    {
        Eigen::Vector3f currentPose = Eigen::Vector3f();
        currentPose[0] = node->getGlobalPose()(0, 3);
        currentPose[1] = node->getGlobalPose()(1, 3);
        currentPose[2] = node->getGlobalPose()(2, 3);
        startPoses.push_back(currentPose);
    }
    rns->setJointValues(endConfig);
    for (RobotNodePtr node : rns->getAllRobotNodes())
    {
        Eigen::Vector3f currentPose = Eigen::Vector3f();
        currentPose[0] = node->getGlobalPose()(0, 3);
        currentPose[1] = node->getGlobalPose()(1, 3);
        currentPose[2] = node->getGlobalPose()(2, 3);
        endPoses.push_back(currentPose);
    }
    for (unsigned int i = 0; i < startPoses.size(); i++)
    {
        double distance = (startPoses[i] - endPoses[i]).norm();
        if (distance > max)
        {
            max = distance;
            best = i;
        }
    }
    return best;

}
