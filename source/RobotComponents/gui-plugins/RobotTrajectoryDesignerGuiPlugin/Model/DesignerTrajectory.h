/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Model
 * @author     Lukas Christ
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef DESIGNERTRAJECTORY_H
#define DESIGNERTRAJECTORY_H


#include <VirtualRobot/RobotNodeSet.h>
#include "UserWaypoint.h"
#include "Transition.h"



namespace armarx
{

    class DesignerTrajectory
    {

    private:
        const unsigned int DERIVATION_COUNT = 2;
        std::vector<TrajectoryPtr> interBreakpointTrajectories;
        std::vector<UserWaypointPtr> userWaypoints;
        std::vector<TransitionPtr> transitions;
        VirtualRobot::RobotNodeSetPtr rns;

        std::vector<std::vector<double>> getDimensionDatas();
        std::vector<double> getAllTimestamps();
        void setLimitless(TrajectoryPtr traj, VirtualRobot::RobotNodeSetPtr rns);


    public:
        DesignerTrajectory(UserWaypointPtr& firstPoint,
                           VirtualRobot::RobotNodeSetPtr newRns);

        /**
         * @brief Deep copy constructor of designerTrajectory
         * @param source designertrajectory to copy
         */
        DesignerTrajectory(const DesignerTrajectory& source);

        /**
         * @brief add a new first userWaypoint
         * @param point userwaypoint
         */
        void addFirstUserWaypoint(UserWaypointPtr& point);

        /**
         * @brief add new last userWaypoint
         * @param point userwaypoint
         */
        void addLastUserWaypoint(UserWaypointPtr& point);

        /**
         * @brief insert userwaypoint before index
         * @param point userwaypoint
         * @param index index
         */
        void insertUserWaypoint(UserWaypointPtr& point, unsigned int index);

        /**
         * @brief delete the userwaypoint and remove all transitions including the userwaypoint.
         * Creates new transition if necessary
         * @param index index of userwaypoint
         */
        void deleteUserWaypoint(unsigned int index);

        /**
         * @brief get the number of the userwaypoints
         * @return number
         */
        unsigned int getNrOfUserWaypoints() const;

        /**
         * @brief get the userWaypoint
         * @param index index of userwaypoint
         * @return the userwaypoint
         */
        UserWaypointPtr getUserWaypoint(unsigned int index);

        /**
         * @brief get the transition
         * @param index index of transition
         * @return the transition
         */
        TransitionPtr getTransition(unsigned int index);

        /**
         * @brief get the time optimal trajectory. Calculated out of all breakpoint trajectories.
         * @return time optimal Trajectory
         */
        TrajectoryPtr getTimeOptimalTrajectory();

        /**
         * @brief get one interBreakPoint trajectory
         * @param index the index
         * @return interBreakpointTrajectorie
         */
        TrajectoryPtr getTrajectorySegment(unsigned int index);

        /**
         * @brief get a copy of all userwaypoints
         * @return all userwaypoints
         */
        std::vector<UserWaypointPtr> getAllUserWaypoints() const;

        /**
         * @brief get all userwaypoints
         * @return all userwaypoints
         */
        std::vector<UserWaypointPtr> getAllUserWaypoints();

        /**
         * @brief get the final trajectory with the right durations
         * @return trajectory
         */
        TrajectoryPtr getFinalTrajectory();

        /**
         * @brief get the robot node set of the designertrajectory
         * @return robotnodeset
         */
        VirtualRobot::RobotNodeSetPtr getRns();

        /**
         * @brief Returns the interBreakpointTrajectories
         * @return interBreakpointTrajectories
         */
        std::vector<TrajectoryPtr> getInterBreakpointTrajectories();


        /**
         * @brief set the robotnodeset
         * @param value robotnodeset
         */
        void setRns(const VirtualRobot::RobotNodeSetPtr& value);

        /**
         * @brief set the interBreakpointTrajectories
         * @param value interBreakpointTrajectories
         */
        void setInterBreakpointTrajectories(const std::vector<TrajectoryPtr>& value);

    };
    typedef std::shared_ptr<DesignerTrajectory> DesignerTrajectoryPtr;
}

#endif // DESIGNERTRAJECTORY_H
