#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::InterpolationSegmentFactory
#define ARMARX_BOOST_TEST

#include <RobotComponents/Test.h>

#include "../InterpolationType.h"
#include "../InterpolationSegmentFactory.h"
#include "../exceptions/NoInterpolationPossibleException.h"
#include "../exceptions/InterpolationNotDefinedException.h"

#include <boost/test/unit_test.hpp>

#include "../../Interpolation/LinearInterpolation.h"
#include "../../Util/OrientationConversion.h"
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
BOOST_AUTO_TEST_CASE(basicTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(0, -2, -8);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose2 = new Pose(pos2, ori2);

    Vector3BasePtr pos3 = new Vector3(5, 8, 7);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    Vector3BasePtr pos4 = new Vector3(4, 23, 1);
    QuaternionBasePtr ori4 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose4 = new Pose(pos4, ori4);

    Vector3BasePtr pos5 = new Vector3(7, 12, 4);
    QuaternionBasePtr ori5 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose5 = new Pose(pos5, ori5);

    Vector3BasePtr pos6 = new Vector3(7, 44, -3);
    QuaternionBasePtr ori6 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose6 = new Pose(pos6, ori6);

    Vector3BasePtr pos7 = new Vector3(5, 4, -8);
    QuaternionBasePtr ori7 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose7 = new Pose(pos7, ori7);

    Vector3BasePtr pos8 = new Vector3(5, -4, -8);
    QuaternionBasePtr ori8 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose8 = new Pose(pos8, ori8);

    Vector3BasePtr pos9 = new Vector3(0, 0, 4);
    QuaternionBasePtr ori9 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose9 = new Pose(pos9, ori9);

    Vector3BasePtr pos10 = new Vector3(0, 1, 2);
    QuaternionBasePtr ori10 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose10 = new Pose(pos10, ori10);

    Vector3BasePtr pos11 = new Vector3(0, 0, 0);
    QuaternionBasePtr ori11 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose11 = new Pose(pos11, ori11);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3, pose4, pose5, pose6, pose7, pose8, pose9, pose10, pose11};

    std::vector<InterpolationType>  interpolations  = {InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eSplineInterpolation
                                                      };

    std::vector<AbstractInterpolationPtr> ip = InterpolationSegmentFactory::produceInterpolationSegments(cp, interpolations);

    AbstractInterpolationPtr ips1 = ip[0];
    AbstractInterpolationPtr ips2 = ip[1];
    AbstractInterpolationPtr ips3 = ip[2];
    AbstractInterpolationPtr ips4 = ip[3];
    AbstractInterpolationPtr ips5 = ip[4];
    AbstractInterpolationPtr ips6 = ip[5];
    AbstractInterpolationPtr ips7 = ip[6];
    AbstractInterpolationPtr ips8 = ip[7];
    AbstractInterpolationPtr ips9 = ip[8];
    AbstractInterpolationPtr ips10 = ip[9];

    BOOST_CHECK_EQUAL(ips1->getPoseAt(0)->position->x, 2);
    BOOST_CHECK_EQUAL(ips1->getPoseAt(0)->position->y, 4);
    BOOST_CHECK_EQUAL(ips1->getPoseAt(0)->position->z, 6);
    BOOST_CHECK_EQUAL(ips1->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ips1->getPoseAt(1)->position->y, -2);
    BOOST_CHECK_EQUAL(ips1->getPoseAt(1)->position->z, -8);

    BOOST_CHECK_EQUAL(ips2->getPoseAt(0)->position->x, 0);
    BOOST_CHECK_EQUAL(ips2->getPoseAt(0)->position->y, -2);
    BOOST_CHECK_EQUAL(ips2->getPoseAt(0)->position->z, -8);
    BOOST_CHECK_EQUAL(ips2->getPoseAt(1)->position->x, 5);
    BOOST_CHECK_EQUAL(ips2->getPoseAt(1)->position->y, 8);
    BOOST_CHECK_EQUAL(ips2->getPoseAt(1)->position->z, 7);

    BOOST_CHECK_EQUAL(ips3->getPoseAt(0)->position->x, 5);
    BOOST_CHECK_EQUAL(ips3->getPoseAt(0)->position->y, 8);
    BOOST_CHECK_EQUAL(ips3->getPoseAt(0)->position->z, 7);
    BOOST_CHECK_EQUAL(ips3->getPoseAt(1)->position->x, 4);
    BOOST_CHECK_EQUAL(ips3->getPoseAt(1)->position->y, 23);
    BOOST_CHECK_EQUAL(ips3->getPoseAt(1)->position->z, 1);

    BOOST_CHECK_EQUAL(ips4->getPoseAt(0)->position->x, 4);
    BOOST_CHECK_EQUAL(ips4->getPoseAt(0)->position->y, 23);
    BOOST_CHECK_EQUAL(ips4->getPoseAt(0)->position->z, 1);
    BOOST_CHECK_EQUAL(ips4->getPoseAt(1)->position->x, 7);
    BOOST_CHECK_EQUAL(ips4->getPoseAt(1)->position->y, 12);
    BOOST_CHECK_EQUAL(ips4->getPoseAt(1)->position->z, 4);

    BOOST_CHECK_EQUAL(ips5->getPoseAt(0)->position->x, 7);
    BOOST_CHECK_EQUAL(ips5->getPoseAt(0)->position->y, 12);
    BOOST_CHECK_EQUAL(ips5->getPoseAt(0)->position->z, 4);
    BOOST_CHECK_EQUAL(ips5->getPoseAt(1)->position->x, 7);
    BOOST_CHECK_EQUAL(ips5->getPoseAt(1)->position->y, 44);
    BOOST_CHECK_EQUAL(ips5->getPoseAt(1)->position->z, -3);

    BOOST_CHECK_EQUAL(ips6->getPoseAt(0)->position->x, 7);
    BOOST_CHECK_EQUAL(ips6->getPoseAt(0)->position->y, 44);
    BOOST_CHECK_EQUAL(ips6->getPoseAt(0)->position->z, -3);
    BOOST_CHECK_EQUAL(ips6->getPoseAt(1)->position->x, 5);
    BOOST_CHECK_EQUAL(ips6->getPoseAt(1)->position->y, 4);
    BOOST_CHECK_EQUAL(ips6->getPoseAt(1)->position->z, -8);

    BOOST_CHECK_EQUAL(ips7->getPoseAt(0)->position->x, 5);
    BOOST_CHECK_EQUAL(ips7->getPoseAt(0)->position->y, 4);
    BOOST_CHECK_EQUAL(ips7->getPoseAt(0)->position->z, -8);
    BOOST_CHECK_EQUAL(ips7->getPoseAt(1)->position->x, 5);
    BOOST_CHECK_EQUAL(ips7->getPoseAt(1)->position->y, -4);
    BOOST_CHECK_EQUAL(ips7->getPoseAt(1)->position->z, -8);

    BOOST_CHECK_EQUAL(ips8->getPoseAt(0)->position->x, 5);
    BOOST_CHECK_EQUAL(ips8->getPoseAt(0)->position->y, -4);
    BOOST_CHECK_EQUAL(ips8->getPoseAt(0)->position->z, -8);
    BOOST_CHECK_EQUAL(ips8->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ips8->getPoseAt(1)->position->y, 0);
    BOOST_CHECK_EQUAL(ips8->getPoseAt(1)->position->z, 4);

    BOOST_CHECK_EQUAL(ips9->getPoseAt(0)->position->x, 0);
    BOOST_CHECK_EQUAL(ips9->getPoseAt(0)->position->y, 0);
    BOOST_CHECK_EQUAL(ips9->getPoseAt(0)->position->z, 4);
    BOOST_CHECK_EQUAL(ips9->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ips9->getPoseAt(1)->position->y, 1);
    BOOST_CHECK_EQUAL(ips9->getPoseAt(1)->position->z, 2);

    BOOST_CHECK_EQUAL(ips10->getPoseAt(0)->position->x, 0);
    BOOST_CHECK_EQUAL(ips10->getPoseAt(0)->position->y, 1);
    BOOST_CHECK_EQUAL(ips10->getPoseAt(0)->position->z, 2);
    BOOST_CHECK_EQUAL(ips10->getPoseAt(1)->position->x, 0);
    BOOST_CHECK_EQUAL(ips10->getPoseAt(1)->position->y, 0);
    BOOST_CHECK_EQUAL(ips10->getPoseAt(1)->position->z, 0);


}

bool is_critical(exceptions::local::NoInterpolationPossibleException const& ex)
{
    return true;
}

bool is_critical2(exceptions::local::InterpolationNotDefinedException const& ex)
{
    return true;
}


BOOST_AUTO_TEST_CASE(wrongSplineTest)
{

    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    Vector3BasePtr pos2 = new Vector3(0, -2, -8);
    QuaternionBasePtr ori2 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose2 = new Pose(pos2, ori2);

    Vector3BasePtr pos3 = new Vector3(5, 8, 7);
    QuaternionBasePtr ori3 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose3 = new Pose(pos3, ori3);

    Vector3BasePtr pos4 = new Vector3(4, 23, 1);
    QuaternionBasePtr ori4 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose4 = new Pose(pos4, ori4);

    Vector3BasePtr pos5 = new Vector3(7, 12, 4);
    QuaternionBasePtr ori5 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose5 = new Pose(pos5, ori5);

    Vector3BasePtr pos6 = new Vector3(7, 44, -3);
    QuaternionBasePtr ori6 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose6 = new Pose(pos6, ori6);

    Vector3BasePtr pos7 = new Vector3(5, 4, -8);
    QuaternionBasePtr ori7 = OrientationConversion::toQuaternion(1, 1, 1);
    PoseBasePtr pose7 = new Pose(pos7, ori7);

    Vector3BasePtr pos8 = new Vector3(5, -4, -8);
    QuaternionBasePtr ori8 = OrientationConversion::toQuaternion(2, 0, -1);
    PoseBasePtr pose8 = new Pose(pos8, ori8);

    Vector3BasePtr pos9 = new Vector3(0, 0, 4);
    QuaternionBasePtr ori9 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose9 = new Pose(pos9, ori9);

    Vector3BasePtr pos10 = new Vector3(0, 1, 2);
    QuaternionBasePtr ori10 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose10 = new Pose(pos10, ori10);

    Vector3BasePtr pos11 = new Vector3(0, 0, 0);
    QuaternionBasePtr ori11 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose11 = new Pose(pos11, ori11);

    std::vector<PoseBasePtr> cp = {pose1, pose2, pose3, pose4, pose5, pose6, pose7, pose8, pose9, pose10, pose11};

    std::vector<InterpolationType>  interpolations  = {InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eLinearInterpolation,
                                                       InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eSplineInterpolation,
                                                       InterpolationType::eSplineInterpolation
                                                      };


    interpolations  = {InterpolationType::eSplineInterpolation,
                       InterpolationType::eSplineInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eSplineInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eSplineInterpolation,
                       InterpolationType::eSplineInterpolation,
                       InterpolationType::eSplineInterpolation
                      };

    interpolations  = {InterpolationType::eSplineInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eLinearInterpolation,
                       InterpolationType::eSplineInterpolation
                      };
    //If no exception is thrown here InterpolationSegmentFactory automatically replaces wrong spline Interpolations with LinearInterpolations
}

BOOST_AUTO_TEST_CASE(basicDominanceTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    std::vector<PoseBasePtr> poses = std::vector<PoseBasePtr>();
    for (int i = 0; i < 10; i++)
    {
        poses.push_back(pose1);
    }
    std::vector<VirtualRobot::IKSolver::CartesianSelection> selections = std::vector<VirtualRobot::IKSolver::CartesianSelection>();
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    InterpolationSegmentFactory::optimizeControlPoints(poses, selections);


    BOOST_CHECK_EQUAL(selections[0], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[1], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[2], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[3], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[4], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[5], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[6], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[7], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[8], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[9], VirtualRobot::IKSolver::CartesianSelection::All);


}

BOOST_AUTO_TEST_CASE(advancedDominanceTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    std::vector<PoseBasePtr> poses = std::vector<PoseBasePtr>();
    for (int i = 0; i < 11; i++)
    {
        poses.push_back(pose1);
    }
    std::vector<VirtualRobot::IKSolver::CartesianSelection> selections = std::vector<VirtualRobot::IKSolver::CartesianSelection>();
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::All);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);

    InterpolationSegmentFactory::optimizeControlPoints(poses, selections);


    BOOST_CHECK_EQUAL(selections[0], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[1], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[2], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[3], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[4], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[5], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[6], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[7], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[8], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[9], VirtualRobot::IKSolver::CartesianSelection::Z);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);
}

BOOST_AUTO_TEST_CASE(reverseOrderDominanceTest)
{
    Vector3BasePtr pos1 = new Vector3(2, 4, 6);
    QuaternionBasePtr ori1 = OrientationConversion::toQuaternion(0, 0, 0);
    PoseBasePtr pose1 = new Pose(pos1, ori1);

    std::vector<PoseBasePtr> poses = std::vector<PoseBasePtr>();
    for (int i = 0; i < 20; i++)
    {
        poses.push_back(pose1);
    }
    std::vector<VirtualRobot::IKSolver::CartesianSelection> selections = std::vector<VirtualRobot::IKSolver::CartesianSelection>();
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Orientation);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Position);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Z);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::Y);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    selections.push_back(VirtualRobot::IKSolver::CartesianSelection::X);
    InterpolationSegmentFactory::optimizeControlPoints(poses, selections);


    BOOST_CHECK_EQUAL(selections[0], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[1], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[2], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[3], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[4], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[5], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[6], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[7], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[8], VirtualRobot::IKSolver::CartesianSelection::All);
    BOOST_CHECK_EQUAL(selections[9], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[10], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[11], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[12], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[13], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[14], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[15], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[16], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[17], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[18], VirtualRobot::IKSolver::CartesianSelection::Position);
    BOOST_CHECK_EQUAL(selections[19], VirtualRobot::IKSolver::CartesianSelection::X);


}






