/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef SPLINEINTERPOLATIONSEGMENT_H
#define SPLINEINTERPOLATIONSEGMENT_H

#include "SplineInterpolation.h"


namespace armarx
{
    /**
    * @brief The SplineInterpolationSegment class represents the segment of a parent SplineInterpolation between two control points
    */
    class SplineInterpolationSegment : public AbstractInterpolation
    {
    public:
        /**
         * @brief SplineInterpolationSegment creates a new SplineInterpolation
         * @param number the number of the control point at the beginning of the interval (from 0 to #controlPoints-2)
         * @param parent the SplineInterpolation that the created Object is the segment of
         */
        SplineInterpolationSegment(int number, AbstractInterpolationPtr parent);

        /**
         * @brief getPoseAt returns the Pose defined by f(time)
         * @param time a time between 0 and 1 with getPoseAt(0) being the startingPose and getPoseAt(1) being the ending Pose
         * @return the pose of the interpolation-function at time
         */
        PoseBasePtr getPoseAt(double time);

        /**
         * @brief always returns 2 as there are always 2 control points in a spline interpolation segment
         */
        int getNumberOfControlPoints();

    private:
        double poseAccesFactor;
        double poseAccesStart;
        AbstractInterpolationPtr parent;
    };

    typedef std::shared_ptr<SplineInterpolationSegment> SplineInterpolationSegmentPtr;
}

#endif
