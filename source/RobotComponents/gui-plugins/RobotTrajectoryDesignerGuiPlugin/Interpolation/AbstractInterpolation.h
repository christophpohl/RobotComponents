/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ABSTRACTINTERPOLATION_H
#define ABSTRACTINTERPOLATION_H

#include <memory>

#include <RobotAPI/interface/core/PoseBase.h>

namespace armarx
{
    /**
     * @brief The AbstractInterpolation class represents a function f:t->P with P being the space of all poses
     * The function parameters are initialized when using the constructors, so using getPoseAt is fast.
     * The Interpolation can be defined by a series of controlPoints and the Interpolation Type, which are the subclasses of this class.
     */
    class AbstractInterpolation
    {
    public:

        /**
         * @brief getPoseAt returns the Pose defined by f(time)
         * @param time a time between 0 and 1 with getPoseAt(0) being the startingPose and getPoseAt(1) being the ending Pose
         * @return the pose of the interpolation-function at time
         */
        virtual PoseBasePtr getPoseAt(double time) = 0;

        /**
         * @brief getNumberOfControlPoints returns number of controlPoints
         * @return the number of controlPoints that define the interpolation
         */
        int getNumberOfControlPoints();

    protected:
        /**
         * @brief controlPoints the controlPoints that are interpolated between
         */
        std::vector<PoseBasePtr> controlPoints;
        /**
         * @brief init convinience method to construct the basic parts of the interpolation (copying all controlPoints)
         * @param cp the controlPoints toinitialize the Interpolation with
         */
        void init(const std::vector<PoseBasePtr> cp);
        /**
         * @brief calculateOrientationAt calculates the rotation/orientation of the pose at a certain time
         * @param time a time between 0 and 1 with calculateOrientationAt(0) being the starting Orientation and calculateOrientationAt(1) being the ending Orientation
         * @return the orientation at time
         */
        virtual const QuaternionBasePtr calculateOrientationAt(double time);
        /**
         * @brief deepCopy creates a real, independent copy of a PoseBasePtr
         * @param org the PoseBasePtr to be cloned
         * @return the cloned PoseBasePtr
         */
        static PoseBasePtr deepCopy(PoseBasePtr org);
    };

    typedef std::shared_ptr <AbstractInterpolation> AbstractInterpolationPtr;
}

#endif
