/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "LinearInterpolation.h"

#include "../Util/OrientationConversion.h"

///EXCEPTIONS
#include "../exceptions/InterpolationNotDefinedException.h"
#include "../exceptions/NoInterpolationPossibleException.h"
///ARMARX-INCLUDES
#include <RobotAPI/libraries/core/FramedPose.h>

using namespace armarx;
LinearInterpolation::LinearInterpolation(std::vector<PoseBasePtr> controlPoints)
{
    if (controlPoints.size() < 2)
    {
        throw new exceptions::local::NoInterpolationPossibleException(2, controlPoints.size());
    }
    init(controlPoints);
    for (unsigned int i = 0; i < this->controlPoints.size() - 1; i++)
    {
        PoseBasePtr current = this->controlPoints.at(i);
        PoseBasePtr next = this->controlPoints.at(i + 1);
        int deltaX = next->position->x - current->position->x;
        int deltaY = next->position->y - current->position->y;
        int deltaZ = next->position->z - current->position->z;
        connectingVector.push_back(*new Eigen::Vector3f(deltaX, deltaY, deltaZ));
    }
}


PoseBasePtr LinearInterpolation::getPoseAt(double time)
{
    if (time < 0 || time > 1)
    {
        throw new exceptions::local::InterpolationNotDefinedException(time);
    }

    int segment = time * (controlPoints.size() - 1);
    if (time == 1)
    {
        return LinearInterpolation::deepCopy(controlPoints[controlPoints.size() - 1]);
    }
    double segmentRelativeTime = (time - (1.0 / (controlPoints.size() - 1.0)) * segment) * (controlPoints.size() - 1);
    PoseBasePtr current = controlPoints[segment];

    Eigen::Vector3f start = Eigen::Vector3f(current->position->x, current->position->y, current->position->z);
    Eigen::Vector3f transition = connectingVector[segment];
    transition *= segmentRelativeTime;
    Eigen::Vector3f position = start + transition;
    return *new PoseBasePtr(new Pose(*new Vector3BasePtr(new Vector3(position)), calculateOrientationAt(time)));
}


