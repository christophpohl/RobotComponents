/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Interpolation
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AbstractInterpolation.h"


///ARMARX-INCLUDES
#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include "../Util/OrientationConversion.h"
#include "../exceptions/InterpolationNotDefinedException.h"


using namespace armarx;

const QuaternionBasePtr AbstractInterpolation::calculateOrientationAt(double time)
{
    if (time < 0 || time > 1)
    {
        throw new exceptions::local::InterpolationNotDefinedException(time);
    }
    int segment = time * (getNumberOfControlPoints() - 1);
    double segmentRelativeTime = (time - (1.0 / (controlPoints.size() - 1.0)) * segment) * (controlPoints.size() - 1);
    PoseBasePtr current = this->controlPoints.at(segment);
    PoseBasePtr next = this->controlPoints.at(segment + 1);

    Eigen::Quaternion<double> ori1 = OrientationConversion::toEigen(current->orientation);
    Eigen::Quaternion<double> ori2 = OrientationConversion::toEigen(next->orientation);
    QuaternionBasePtr temp = OrientationConversion::toArmarX(ori1.slerp(segmentRelativeTime, ori2));
    return temp;
}

int AbstractInterpolation::getNumberOfControlPoints()
{
    return controlPoints.size();
}

void AbstractInterpolation::init(const std::vector<PoseBasePtr> cp)
{
    for (unsigned int i = 0; i < cp.size(); i++)
    {
        this->controlPoints.push_back(deepCopy(cp[i]));

    }
}

PoseBasePtr AbstractInterpolation::deepCopy(PoseBasePtr org)

{
    QuaternionPtr tempOri = QuaternionPtr(new Quaternion(org->orientation->qw, org->orientation->qx, org->orientation->qy, org->orientation->qz));
    Vector3BasePtr tempPos = Vector3BasePtr(new Vector3(org->position->x, org->position->y, org->position->z));
    return *new PoseBasePtr(new Pose(tempPos, tempOri));
}

