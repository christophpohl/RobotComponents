/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RobotVisualizationWidget.h"

#include "CoinRobotViewerAdapter.h"

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;

RobotVisualizationWidget::RobotVisualizationWidget(QWidget* parent, QWidget* viewerWidget, RobotVisualizationPtr viewer) : QSplitter(parent)
{
    //this->setOpaqueResize(true);
    //initialize Attriutes
    this->selectedViewer = 0;
    this->viewer = viewer;
    this->viewerWidget = viewerWidget;
    viewerWidget->setObjectName("0");
    this->childrenCounter = 1;
    this->leftSplitter = new QSplitter(Qt::Orientation::Vertical, this);
    this->rightSplitter = new QSplitter(Qt::Orientation::Vertical, this);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    //Add callback for viewer selection from Main-Application
    QObject::connect(QApplication::instance(), SIGNAL(focusChanged(QWidget*, QWidget*)), this, SLOT(activeWidgetChanged(QWidget*, QWidget*)));

    //Add original viewer to this widget
    viewerWidget->setFocusPolicy(Qt::FocusPolicy::WheelFocus);
    this->leftSplitter->addWidget(viewerWidget);
}


RobotVisualizationWidget::~RobotVisualizationWidget()
{
    ARMARX_WARNING << "Destroyed ViewerWidget";
}

void RobotVisualizationWidget::addWidget()
{
    if (childrenCounter < 4)
    {
        QWidget* parent = new QWidget();
        clones.push_back(viewer->reproduce(parent));
        parent->setObjectName(QString::fromStdString(std::to_string(childrenCounter)));
        parent->setFocusPolicy(Qt::FocusPolicy::WheelFocus);
        cloneWidgets.push_back(parent);

        if (childrenCounter == 0)
        {
            leftSplitter->addWidget(parent);
        }
        //Remove Viewer Left of original Viewer
        else if (childrenCounter == 1)
        {
            rightSplitter->addWidget(parent);
        }
        //Add new Viewer in the lower right corner
        else if (childrenCounter == 2)
        {
            rightSplitter->addWidget(parent);
        }
        //Add new Viewer in the lower left corner
        else if (childrenCounter == 3)
        {
            leftSplitter->addWidget(parent);
        }
        childrenCounter++;
    }

}

void RobotVisualizationWidget::removeWidget()
{
    //Remove Viewer Left of original Viewer
    if (childrenCounter == 2)
    {
        if (rightSplitter->children().at(0))
        {
            cloneWidgets.pop_back();
            clones.pop_back();
            rightSplitter->children().at(0)->~QObject();
            childrenCounter--;
        }
    }
    //Remove Viewer in the lower right corner
    else if (childrenCounter == 3)
    {
        if (rightSplitter->children().at(1))
        {
            cloneWidgets.pop_back();
            clones.pop_back();
            rightSplitter->children().at(0)->~QObject();
            childrenCounter--;
        }
    }
    //Remove Viewer in lower left corner
    else if (childrenCounter == 4)
    {
        if (leftSplitter->children().at(1))
        {
            cloneWidgets.pop_back();
            clones.pop_back();
            leftSplitter->children().at(0)->~QObject();
            childrenCounter--;

        }
    }
    if (selectedViewer == childrenCounter)
    {
        selectedViewer--;
    }
}

void RobotVisualizationWidget::setCameraOfFocused(Eigen::Vector3f position, Eigen::Vector3f pointAtA, Eigen::Vector3f pointAtB)
{
    if (selectedViewer == 0)
    {
        viewer->setCamera(position, pointAtA, pointAtB);
    }
    else
    {
        ARMARX_INFO << std::to_string(selectedViewer);
        RobotVisualizationPtr clone = clones[selectedViewer - 1];
        clone->setCamera(position, pointAtA, pointAtB);
    }

}

void RobotVisualizationWidget::activeWidgetChanged(QWidget* old, QWidget* now)
{
    if (!now)
    {
        return;
    }
    /*for (int i = 0; i < 2; i++)
    {
        ARMARX_WARNING << now->objectName().toStdString() + "..." + std::to_string(i);
        now = now->nextInFocusChain();
    }*/

    if (now == viewerWidget || (now->parentWidget() && now->parentWidget()->parentWidget() && now->parentWidget()->parentWidget()->parentWidget() && now->parentWidget()->parentWidget()->parentWidget() == viewerWidget))
    {
        selectedViewer = 0;
    }
    int j = 1;
    for (QWidget * clone : cloneWidgets)
    {
        if (now == clone || (now->parentWidget() && now->parentWidget()->parentWidget() && now->parentWidget()->parentWidget()->parentWidget() && now->parentWidget()->parentWidget()->parentWidget() == clone))
        {
            this->selectedViewer = j;
            return;
        }
        j++;
    }
    /*std::string s = now->nextInFocusChain()->objectName().toStdString();
    ARMARX_WARNING << s + "NEXT";
    ARMARX_WARNING << now->objectName().toStdString() + "NOW";
    ARMARX_WARNING << now->parentWidget()->parentWidget()->parentWidget()->objectName().toStdString() + "PARENT";
    for (int i = 0; i < childrenCounter; i++)
    {
        if (s.length() >= 15)
        {
            std::string number = s.substr(15);
            if (std::to_string(i) == number)
            {
                this->selectedViewer = i;
                return;
            }
        }
    }*/
}

