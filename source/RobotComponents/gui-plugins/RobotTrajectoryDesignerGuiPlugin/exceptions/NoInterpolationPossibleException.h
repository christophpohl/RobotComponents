/*
* This file is part of ArmarX.
* 
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved. 
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Timo Birr
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef NOINTERPOLATIONPOSSIBLEEXCEPTION_H
#define NOINTERPOLATIONPOSSIBLEEXCEPTION_H

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>


namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class DynamicLibraryException
              \brief This exception is thrown if an invalid value was specified for a property.
              \ingroup Exceptions
             */
            class NoInterpolationPossibleException: public armarx::LocalException
            {
            public:
                std::string propertyName;
                std::string invalidPropertyValue;

                NoInterpolationPossibleException(const int needed, const int actual) :
                    armarx::LocalException("You need at least " + std::to_string(needed) + "contol points to interpolate but only got " + std::to_string(actual))
                {}

                ~NoInterpolationPossibleException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::NoInterpolationPossibleException";
                }
            };
        }
    }
}
#endif
