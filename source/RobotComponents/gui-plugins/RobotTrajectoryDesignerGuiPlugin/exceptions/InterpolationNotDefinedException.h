/*
* This file is part of ArmarX.
* 
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved. 
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Timo Birr
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef INTERPOLATINNOTDEFINEDEXCEPTION_H
#define INTERPOLATINNOTDEFINEDEXCEPTION_H

#include "ArmarXCore/core/exceptions/Exception.h"

#include "ArmarXCore/core/exceptions/local/ValueRangeExceededException.h"

#include <string>


namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class DynamicLibraryException
              \brief This exception is thrown if an invalid value was specified for a property.
              \ingroup Exceptions
             */
            class InterpolationNotDefinedException: public armarx::LocalException
            {
            public:
                std::string propertyName;
                std::string invalidPropertyValue;

                InterpolationNotDefinedException(const double value) throw() :
                    armarx::LocalException("No Interpolation defined at " + std::to_string(value) + ". Only possible between 0 and 1")
                {}

                ~InterpolationNotDefinedException() throw() { }

                virtual std::string name() const
                {
                    return "armarx::exceptions::local::InterpolationNotDefinedException";
                }
            };
        }
    }
}
#endif
