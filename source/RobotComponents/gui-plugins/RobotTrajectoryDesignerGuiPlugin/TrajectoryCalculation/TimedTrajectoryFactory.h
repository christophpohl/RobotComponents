#ifndef TIMEDTRAJECTORYFACTORY_H
#define TIMEDTRAJECTORYFACTORY_H

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/TimeOptimalTrajectory/TimeOptimalTrajectory.h>
#include "TimedTrajectory.h"

namespace armarx
{
    class TimedTrajectoryFactory;
    /**
     * @class TimedTrajectoryFactory
     * @brief Creates a TimedTrajectory out of a TimeOptimalTrajectory supplied by Simox methods.
    */
    class TimedTrajectoryFactory
    {
    public:
        /**
         * @brief Creates a TimedTrajectory out of the TimeOptimalTrajectory and maps the parameter userPoints to it.
         * @param trajectory The nodes of the trajectory.
         * @param userPoints The userPoints that have to be mapped.
         * @param maxDeviation The maximum deviation with which the userPoints may vary from the input TimedTrajectory.
         * @return A TimedTrajectory which contains the timestamps of the supplied userPoints.
         */
        static TimedTrajectory createTimedTrajectory(VirtualRobot::TimeOptimalTrajectory& trajectory, std::vector<std::vector<double>>& userPoints, VirtualRobot::RobotNodeSetPtr rns, double maxDeviation);
    };
}
#endif // TIMEDTRAJECTORYFACTORY_H
