/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef PATHFACTORY_H
#define PATHFACTORY_H

#include <Eigen/Core>
#include <VirtualRobot/TimeOptimalTrajectory/Path.h>
#include "../Model/DesignerTrajectory.h"

namespace armarx
{
    class PathFactory;
    /**
     * @class PathFactory
     * @brief Creates a Simox VirtualRobot::Path from a set of nodes representing joint angles and a maximum deviation parameter.
    */
    class PathFactory
    {
    public:
        /**
         * @brief Creates a Path out of the nodes with a maximum deviation.
         * @param nodes The nodes the path goes through.
         * @param maxDeviation The maximum deviation of the points along the path.
         * @return The created path.
         */
        static VirtualRobot::Path createPath(std::vector<std::vector<double>>& nodes, double maxDeviation);
    };
}
#endif // PATHFACTORY_H

