#define BOOST_TEST_MODULE ArmarX::RobotTrajectoryDesigner::TimedTrajectoryFactory
#define ARMARX_BOOST_TEST

#include "../../../../../build/testing/RobotTrajectoryDesigner/Test.h"
#include "../Environment.h"
#include "../TimedTrajectoryFactory.h"
#include "../PathFactory.h"
using namespace armarx;

//Check if the trajectory contained in the returned timedtrajectory is correct
BOOST_AUTO_TEST_CASE(CorrectTrajectoryTest)
{
    BOOST_CHECK_EQUAL(1, 1);/*
    VirtualRobot::RobotNodeSetPtr rns = Environment::getRobot()->getRobotNodeSets()[0];
    std::vector<std::vector<double>> data;
    for(unsigned int i = 0; i < 10; i++){
        std::vector<double> nodeData;
        for(unsigned int j = 0; j < rns->getAllRobotNodes().size(); j++){
            nodeData.push_back(i);
        }
            data.push_back(nodeData);
    }
    VirtualRobot::Path p = PathFactory::createPath(data, 0.0);
    Eigen::VectorXd maxVelocity;
    Eigen::VectorXd maxAcceleration;
    VirtualRobot::TimeOptimalTrajectory t = VirtualRobot::TimeOptimalTrajectory(p, maxVelocity, maxAcceleration, 0.001);
    TimedTrajectory trajectory = TimedTrajectoryFactory::createTimedTrajectory(t, data, rns, 0.0);*/
}
/*
//Check if the userPoints contained in the returned timedtrajectory are correct
BOOST_AUTO_TEST_CASE(CorrectUserPointsTest){

}

//Check if the userPoints contained in the returned timedtrajectory fit to the trajectory itself
BOOST_AUTO_TEST_CASE(CorrectMappingTest){

}
*/
//Check if an incorrect rns leads to the correct exception
/*
BOOST_AUTO_TEST_CASE(IncorrectRobotNodeSetTest)
{
    //Set robot for testing purposes.
    if (Environment::getRobot() == NULL)
    {
        return;
    }
    std::vector<VirtualRobot::RobotNodePtr> nodes = Environment::getRobot()->getRobotNodes();
    VirtualRobot::RobotNodeSetPtr rns = VirtualRobot::RobotNodeSet::createRobotNodeSet(
                                            Environment::getRobot(), "Incorrect RobotNodeSet", nodes,
                                            VirtualRobot::RobotNodePtr(), VirtualRobot::RobotNodePtr(), false);
    std::vector<std::vector<double>> data;
    /*
    for(unsigned int i = 0; i < 10; i++){
        std::vector<double> nodeData;
        for(unsigned int j = 0; j < rns->getAllRobotNodes().size(); j++){
            nodeData.push_back(i);
        }
            data.push_back(nodeData);
    }*/
/*
    VirtualRobot::Path p = PathFactory::createPath(data, 0.0);
    Eigen::VectorXd maxVelocity;
    Eigen::VectorXd maxAcceleration;
    VirtualRobot::TimeOptimalTrajectory t = VirtualRobot::TimeOptimalTrajectory(p, maxVelocity, maxAcceleration, 0.001);
    BOOST_CHECK_THROW(TimedTrajectoryFactory::createTimedTrajectory(t, data, rns, 0.0), NotImplementedYetException);

}
*/
