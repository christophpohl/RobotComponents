/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef MMMEXPORTER_H
#define MMMEXPORTER_H
#include "../Model/DesignerTrajectory.h"
#include "../Environment.h"
namespace armarx
{
    class MMMExporter;
    /**
     * @class MMMExporter
     * @brief Exports instances of the DesignerTrajectory class to .xml-files in MMM format.
     */
    class MMMExporter/* : public DesignerExporter*/
    {
    private:
        EnvironmentPtr environment;
    public:
        MMMExporter(EnvironmentPtr environment);
        /**
         * @brief Exports a trajectory to the target file as a MMM file.
         * @param trajectory Trajectory to export.
         * @param fps Frequency of the measurements in measurements per second.
         */
        void exportTrajectory(std::vector<armarx::DesignerTrajectoryPtr> trajectories, const std::string file);
    };

    typedef std::shared_ptr<MMMExporter> MMMExporterPtr;
}
#endif // MMMEXPORTER_H
