/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef TRAJECTORYIMPORTER_H
#define TRAJECTORYIMPORTER_H
#include <Ice/Ice.h>
#include "DesignerImporter.h"
#include "../Environment.h"
namespace armarx
{
    class TrajectoryImporter;
    /**
     * @class TrajectoryImporter
     * @brief Imports instances of the DesignerTrajectory class out of a serialized instance of the Trajectory class from a target file.
     */
    class TrajectoryImporter : public DesignerImporter
    {
    private:
        EnvironmentPtr environment;

    public:
        TrajectoryImporter(EnvironmentPtr environment);
        /**
         * @brief Import a serialized instance of the Trajectory class from the target file as a trajectory.
         */
        std::vector<DesignerTrajectoryPtr> importTrajectory(std::string file);
    };

    typedef std::shared_ptr<TrajectoryImporter> TrajectoryImporterPtr;
}
#endif // TRAJECTORYIMPORTER_H
