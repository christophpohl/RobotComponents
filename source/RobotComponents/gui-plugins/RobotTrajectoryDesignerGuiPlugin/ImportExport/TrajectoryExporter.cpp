/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::ImportExport
 * @author     Liran Dattner
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TrajectoryExporter.h"
#include <RobotAPI/libraries/core/Trajectory.h>
#include <iostream>
#include <fstream>
#include <ArmarXCore/interface/observers/Serialization.h>
#include <ArmarXCore/interface/serialization/JSONSerialization.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <Ice/CommunicatorF.h>

//@Liran ich (Tim) habe es von DesignerTrajectory zu DesignerTrajectoryPtr geändert. Ich bekomme es so von dem Model
//fps habe ich wie besprochen entfernt
void armarx::TrajectoryExporter::exportTrajectory(std::vector<DesignerTrajectoryPtr> trajectories, const std::string file)
{
    const Ice::Current& c = Ice::Current();
    auto communicator = c.adapter.get()->getCommunicator();
    Ice::CommunicatorPtr ic = Ice::CommunicatorPtr(communicator);
    JSONObject jsonSerializer = JSONObject(ic);
    //Serialize the trajectory using jsonSerializer
    //trajectory->getFinalTrajectory()->serialize(&jsonSerializer);
    //Output the serialized Trajectory as a string
    const std::string jsonString = jsonSerializer.toString();

    //Stream the string to a file
    std::ofstream output;
    output.open(file);
    output << jsonString;
    output.close();
}
