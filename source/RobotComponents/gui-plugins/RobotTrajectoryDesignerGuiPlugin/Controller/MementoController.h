/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::MementoController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef MEMENTOCONTROLLER_H
#define MEMENTOCONTROLLER_H
#include "AbstractController.h"
#include <QPushButton>

namespace armarx
{
    /**
     * @class MementoController
     * @brief Subcontroller which handles all user interaction with the undo and redo
     *          buttons on the user interface. Communicates with other controllers to
     *          realize the memento pattern.
     */
    class MementoController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new MementoController with two given push buttons
         *          realizing the undo and redo operations
         * @param undoButton Push button realizing the undo operation
         * @param redoButton Push button realizing the redo operation
         */
        MementoController(QPushButton* undoButton, QPushButton* redoButton);

        /**
         * @brief Getter for the undo button
         * @return Push button realizing the undo operation
         */
        QPushButton* getUndoButton();

        /**
         * @brief Getter for the redo button
         * @return Push button realizing the redo operation
         */
        QPushButton* getRedoButton();

    public slots:
        /**
         * @brief Enables or disables the undo button
         * @param enable Determines whether to enable or disable the undo button
         */
        void enableUndoButton(bool enable);

        /**
         * @brief Enables or disables the redo button
         * @param enable Determines whether to enable or disable the redo button
         */
        void enableRedoButton(bool enable);

        /**
         * @brief Enables or disables the redo button
         * @param enable Determines whether to enable or disable the redo button
         */
        void enableRedoButtonVisualization(bool enable);

    private slots:
        /**
         * @brief Undoes the lastly executed operation
         */
        void undoOperation();

        /**
         * @brief Redoes the lastly undone operation
         */
        void redoOperation();

    signals:
        /**
         * @brief Notifies other controllers about undoing the lastly executed operation
         */
        void undo();

        /**
         * @brief Notifies other controllers about redoing the lastly undone operation
         */
        void redo();

    private:
        QPushButton* undoButton;
        QPushButton* redoButton;
        bool redoBool;
    };

    typedef std::shared_ptr<MementoController> MementoControllerPtr;
}

#endif // MEMENTOCONTROLLER_H
