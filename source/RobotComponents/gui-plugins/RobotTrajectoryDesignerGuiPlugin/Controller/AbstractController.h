/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::AbstractController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef ABSTRACTCONTROLLER_H
#define ABSTRACTCONTROLLER_H
#include <QObject>
#include <QErrorMessage>
#include <memory>
#include "ArmarXCore/core/logging/Logging.h"

namespace armarx
{
    /**
     * @brief Abstract controller providing a set of methods which must be
     *          implemented by every controller
     */
    class AbstractController : public QObject
    {
        Q_OBJECT

    public:
        /**
         * @brief Initializes the controller
         */
        virtual void onInitComponent() = 0;

        /**
         * @brief Connects all signals and slots of the controller
         */
        virtual void onConnectComponent() = 0;

        /**
         * @brief Called whenever a component is disconnected
         */
        virtual void onDisconnectComponent() = 0;

        /**
         * @brief Called on exit, cleans
         */
        virtual void onExitComponent() = 0;
    };

    typedef std::shared_ptr<AbstractController> AbstractControllerPtr;
}

#endif // ABSTRACTCONTROLLER_H
