#ifndef ENVIRONMENTCONTROLLER_H
#define ENVIRONMENTCONTROLLER_H
#include "AbstractController.h"
#include "../Environment.h"
#include <RobotAPI/interface/core/RobotState.h>

namespace armarx
{
    class EnvironmentController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief loads first armar3 in the application
         */
        void loadArmar3();

        /**
         * @brief loads the robot from proxy
         * @param robotFileName file name of the robot
         */
        void loadRobotFromProxy(std::string robotFileName);

        EnvironmentController();

    public slots:
        /**
         * @brief open a file dialog to select a robot
         */
        void openRobotFileDialog();

    signals:
        /**
         * @brief notifies all controller that the environment has changed
         * @param environment
         */
        void environmentChanged(EnvironmentPtr environment);

    protected:
        RobotStateComponentInterfacePrx robotStateComponentPrx;

    private:
        VirtualRobot::RobotPtr loadRobot(Ice::StringSeq includePaths);
        EnvironmentPtr environment;
    };

    typedef std::shared_ptr<EnvironmentController> EnvironmentControllerPtr;
}

#endif // ENVIRONMENTCONTROLLER_H
