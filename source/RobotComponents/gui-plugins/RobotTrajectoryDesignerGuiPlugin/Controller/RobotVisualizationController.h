/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGuiPlugins::RobotTrajectoryDesigner::Controllers
 * @author     Timo Birr
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef ROBOTVIEWERCONTROLLER_H
#define ROBOTVIEWERCONTROLLER_H

#include "AbstractController.h"
#include "RobotAPI/interface/core/PoseBase.h"
#include <Eigen/Eigen>
#include "RobotAPI/libraries/core/Trajectory.h"
#include <../Visualization/RobotVisualization.h>
#include "../Visualization/DesignerTrajectoryPlayer.h"
#include "../Model/DesignerTrajectory.h"
#include "VirtualRobot/IK/IKSolver.h"
#include "../Visualization/CoinRobotViewerAdapter.h"
#include "../Visualization/RobotVisualizationWidget.h"
#include <Inventor/sensors/SoTimerSensor.h>
#include "../Visualization/VisualizationObserver.h"
#include "../Environment.h"

namespace armarx
{

    /**
     * @brief The RobotVisualizationController
     * A Controller to control the visualization of the robot. Hides the concrete visualization Method(Coin,OSG) from the rest of the application.
     * Provides Methods for adding Visualization of one Robot at a time, waypoints and transitions between waypoints. Supports up to four parallel views.
     * Informs other Controllers about the Position of the Manipulator, the TCP and the selected waypoint.
     */
    class RobotVisualizationController : public AbstractController , public VisualizationObserver
    {
        Q_OBJECT
        friend class RobotVisualization;

    public:
        /**
         * @brief RobotVisualizationController creates a new RobotVisualizationController
         * @param parent the widget in which th viewer should be placed in
         */
        RobotVisualizationController(QWidget* parent);
        ~RobotVisualizationController();

        //inherited by abstract controller
        void onInitComponent();
        void onConnectComponent();
        void onDisconnectComponent();
        void onExitComponent();

        //inherited by VisualizationObserver
        void refresh();
        void addConnection(std::shared_ptr<RobotVisualizationController> ctr);

    public slots:

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// PARALLEL VIEWS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief addView adds new RobotViewer
         */
        void addView();
        /**
         * @brief removeView removes RobotViewer with index
         * @param index the index of the view to remove
         */
        void removeView();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// UPDATING OF VISUALIZATION
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief updateViews Removes the current visualization of all Waypoints and Transitions and replaces them with the visualization of the new Trajectory
         * @param trajectory the trajectory to display next
         */
        void updateViews(DesignerTrajectoryPtr trajectory);

        /**
         * @brief robotChanged updates the robot model that is currently visualized
         * Removes the robot that is visualized prior to this one
         * @param robot the new Robot to visualize
         */
        void robotChanged(VirtualRobot::RobotPtr robot);

        /**
         * @brief environmentChanged updates the environment that is currently visualized
         * @param environment the new environment
         */
        void environmentChanged(EnvironmentPtr environment);

        /**
         * @brief displayAllWayPoints Enables Visualization and threfore selection of all waypoints of the current Trajectory and not only userWaypoints
         * @param display
         */
        void displayAllWayPoints(bool display);

        /**
         * @brief selectedWayPointChanged highlights the waypoint with index and moves the manipulator to its Pose
         * @param index the index of the waypoint to select next
         */
        void selectedWayPointChanged(int index);

        /**
         * @brief selectedTransitionChanged highlights the Transition with index
         * @param index the index of the transition that should be highlighted
         */
        void selectedTransitionChanged(int index);

        void clearView();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// IK CALLBACK METHODS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief cartesianSelectionChanged recalculates the configuration of the robot joints to fit the new selection
         * @param cs the new CartesianSelection
         */
        void cartesianSelectionChanged(VirtualRobot::IKSolver::CartesianSelection cs);

        /**
         * @brief kinematicChainChanged removes all visualization from the previously selected Trajectory and changes the Visualization of the
         * Manipulator to look like the endeffector of the kinematic chain
         * @param selectedChain
         */
        void kinematicChainChanged(VirtualRobot::RobotNodeSetPtr rns);

        /**
         * @brief setIKCallbackEnabled sets whther the robot Model tries to reach the Pose determined by the Manipulator
         * @param enabled if true the robot follows the manipulator
         */
        void setIKCallbackEnabled(bool enabled);




        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// CAMERA UPDATING METHODS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief setCamera Sets the camera of all views to standard Position
         * sets camera to standard perspective
         */
        void setCamera();

        /**
         * @brief setCamera sets camera to the Perspective with index perspective in the corresponding enum
         * @param perspective the selected perspective
         * 0 - High Angle
         * 1 - Top
         * 2 - Front
         * 3 - Back
         * 4 - Left
         * 5 - Right
         */
        void setCamera(int perspective);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// PLAY TRAJECTORY
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
         * @brief playTrajectories plays the currently selected trajectory and disables all other visualization features
         */
        void playTrajectory();
        /**
         * @brief playTrajectories plays all trajectories at the same time
         * @param trajectories the trajectories to play - no duplicates of selected robot node sets allowed!
         */
        void playTrajectories(std::vector<DesignerTrajectoryPtr> trajectories);
        /**
         * @brief trajectoryPlayerStopped restarts the visualization of the whole scene after the debug drawer does not longer visualize the trajectory
         */
        void trajectoryPlayerStopped();




    signals:
        /**
         * @brief wayPointSelected informs all relevant controllers that the currently selected Waypoit has been changed via click on the waypoint in the RobotViewer
         * @param index
         */
        void wayPointSelected(int index);
        /**
         * @brief manipulatorChanged informs all relevant controllers that the pose of the manipulator has changed and therefore the currently selected Waypoint is also changed
         */
        void manipulatorChanged(Eigen::Matrix4f globalPose);
        /**
         * @brief tcpPoseChanged informs all releveant controllers about the new global pose of the manipulator
         * @param globalPose the pose of the manipulator relative to the global frame
         */
        void tcpPoseChanged(Eigen::Matrix4f globalPose);
        /**
         * @brief poseReachable informs all relevant controllers whether the pose of the manipulator is reachable
         * @param reachable true when there is an IK-solution for the manipulator could be found
         */
        void poseReachable(bool reachable);
        /**
         * @brief trajectoryPlayerNotPlaying
         * @param playing
         */
        void trajectoryPlayerNotPlaying(bool playing);

        void trajectoryPlayerPlaying(bool playing);

    private:

        RobotVisualizationPtr viewer;
        QWidget* parent;
        RobotVisualizationWidget* viewSplitter;
        bool iKCallback;
        DesignerTrajectoryPtr currentTrajectory;
        VirtualRobot::RobotPtr robot;
        int selectedWayPoint;
        int selectedTransition;
        VirtualRobot::IKSolver::CartesianSelection cs;
        VirtualRobot::RobotNodeSetPtr selectedKinematicChain;
        EnvironmentPtr environment;

        bool playerRunning;
        DesignerTrajectoryPlayerPtr player;
        void playerStarter();
    };


    typedef std::shared_ptr<RobotVisualizationController> RobotVisualizationControllerPtr;
}

#endif
