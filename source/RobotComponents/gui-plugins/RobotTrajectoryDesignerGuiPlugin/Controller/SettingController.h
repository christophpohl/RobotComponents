/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::SettingController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef SETTINGCONTROLLER_H
#define SETTINGCONTROLLER_H
#include "AbstractController.h"
#include "../View/SettingTab.h"
#include "../Environment.h"
#include "VirtualRobot/RobotNodeSet.h"
#include "../Util/WheelEventFilter.h"

#include <memory>
#include <QComboBox>
#include <QVariant>

namespace armarx
{
    /**
     * @class SettingController
     * @brief Subcontroller which handles all user interaction with the setting
     *          tab in the GUI, communicates with other controllers via signals
     *          and slots
     */
    class SettingController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new SettingController and assigns a SettingTab to handle
         * @param guiSettingTab Pointer to the SettingTab whose user interaction
         *                      is handled by the TransitionController
         */
        SettingController(SettingTabPtr guiSettingTab);

        /**
         * @brief Getter for the SettingTab pointer to guiSettingTab
         * @return A Pointer to the guiSettingTab
         */
        SettingTabPtr getGuiSettingTab();

        /**
         * @brief Setter for the SettingTab pointer to guiSettingTab
         * @param guiSettingTab Pointer to the SettingTab whose user interaction
         *                         is handled by the SettingController
         */
        void setGuiSettingTab(SettingTabPtr guiSettingTab);

    public slots:
        /**
         * @brief Changes the currently selected TCP
         * @param index Index of the current TCP
         */
        void selectTCP(int index);

        /**
         * @brief Changes the currently selected TCP
         * @param tcp String identifier of the current TCP
         */
        void selectTCP(QString tcp);

        /*
         *
         * Check with waypoint controller how many waypoints there are to disable or
         * enable the ik solution button. (Signal is emitted from waypoint controller)
         *
         */
        /**
         * @brief Enables or disables the new IK solution button
         * @param enable Determines whether to enable or disable the ik solution button
         */
        void enableIKSolutionButton(bool enable);

        /**
         * @brief Enables or disables the export buttons
         * @param enable Determines whether to enable or disable the export buttons
         */
        void enableExportButtons(bool enable);

        /**
         * @brief Enables or disables the import, tcp and collision buttons
         * @param enable Determines whether to enable or disable the import, tcp and collision buttons
         */
        void enableImportTCPCollision(bool enable);

        /**
         * @brief Retranslates the guiSettingTab
         */
        void retranslateGui();


        void enableSelectRobotButton(bool enable);
        /**
         * @brief Set the enviroment
         * @param The new enviroment
         */
        void environmentChanged(EnvironmentPtr environment);

    private slots:
        /**
         * @brief Opens a dialog to select which robot to use in the application
         */
        void openRobotSelectionDialog();

        /**
         * @brief Opens a dialog to access all keyboard shortcuts provided by
         *          the RobotTrajectoryDesigner
         */
        void openShortcutDialog();

        /*
         *
         * Calculates a new IK solution for the first waypoint of the selected kinematic
         * chain by calling the kinematic solver.
         * Then add joint angles to current robot node set and send signal to visualization
         *
         */
        /**
         * @brief Calculates a new IK solution for the first waypoint of the selected
         *          kinematic chain and applies it to the robot
         */
        void newIKSolution();

        /**
         * @brief Changes the active collision model for the selected TCP
         * @param index Index of the active collision model
         */
        void selectActiveCM(int index);

        /**
         * @brief Checks if the checkstate of the given item has changed. In that case,
         *        all used collision models are updated internally.
         * @param item List widget item whose checkstate might have changed
         */
        void setCollisionModelList(QListWidgetItem* item);

        /**
         * @brief Export all trajectory to Trajectory
         */
        void exportTrajectorySlot();

        /**
         * @brief Convert all trajectory to MMM
         */
        void convertToMMMSlot();

        /**
         * @brief Opens a dialog to import an existing trajectory
         */
        void openImportDialog();

        /**
         * @brief Sets the display language that is to be used in the GUI
         * @param index Index of the language
         */
        void setLanguage(int index);

    signals:
        /**
         * @brief Notifies other controllers about a change of the language
         * @param index Index of the language
         */
        void changedLanguage(QString language);

        /**
         * @brief Notifies other controllers about a change of the current TCP
         * @param index Index of the TCP
         */
        void changedTCP(QString tcp);

        /**
         * @brief Sets the active collision model name
         * @param activeColModelName Active collision model name
         */
        void setActiveColModelName(QString activeColModelName);

        /**
         * @brief Sets the body collision models names
         * @param activeColModelName Body collision models names
         */
        void setBodyColModelsNames(QStringList bodyColModelsNames);

        /**
         * @brief Notifies other controllers to open a robot selection dialog
         */
        void openRobotSelection();

        /**
         * @brief Notifies other controllers to open a shortcut dialog
         */
        void openShortcut();

        /**
         * @brief Notifies other controllers to export all trajectories to Trajectory
         */
        void exportTrajectory();

        /**
         * @brief Notifies other controllers to convert all trajectories to MMM
         */
        void convertToMMM();

        /**
         * @brief Notifies other controllers to open an import dialog
         */
        void openImport();

        /*
         *
         * Notify other controllers about a new ik solution. (visualization, trajectory)
         *
         */
        /**
         * @brief Notifies other controllers about the availability of a new IK
         *        solution for the first waypoint
         */
        void changedIKSolution();

    private:
        SettingTabPtr guiSettingTab;
        EnvironmentPtr environment;

        /**
         * @brief Enables or Disables the collision model combo box and list, export and
         *          import buttons
         * @param enable Determines whether to enable or disable the widgets
         */
        void enableWidgets(bool enable);

        /**
         * @brief Initializes the contents of the TCP combo box with robot
         *          node sets from a given robot
         * @param robot Pointer to the used robot
         */
        void initTCPComboBox(VirtualRobot::RobotPtr robot);

        /**
         * @brief Initializes the contents of the active collision model combo
         *          box with collision models from a given robot
         * @param robot Pointer to the used robot
         */
        void initCMComboBox(VirtualRobot::RobotPtr robot);

        /**
         * @brief Initializes the contents of the collision model list with
         *          collision models from a given robot
         * @param robot Pointer to the used robot
         */
        void initCMList(VirtualRobot::RobotPtr robot);
    };

    typedef std::shared_ptr<SettingController> SettingControllerPtr;
}


#endif // SETTINGCONTROLLER_H
