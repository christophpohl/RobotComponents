/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TransitionController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef TRANSITIONCONTROLLER_H
#define TRANSITIONCONTROLLER_H
#include "AbstractController.h"
#include "../View/TransitionTab.h"
#include "../Util/WheelEventFilter.h"

#include <QListWidget>
#include <QMetaType>
#include <QVariant>
#include <QDoubleValidator>


namespace armarx
{
    /**
     * @brief Struct defining a transition which can be stored as QVariant
     *          in a QListWidgetItem
     */
    typedef struct GuiTransition
    {
        double duration;
        double start;
        int it;

        /**
         * @brief Overload '==' operator for GuiTransition struct
         * @param transition GuiTransition to compare with
         * @return true, iff all values of both GuiTransitions are equal, otherwise false
         */
        bool operator==(GuiTransition transition)
        {
            return (duration == transition.duration)
                   && (start == transition.start)
                   && (it == transition.it);
        }

        /**
         * @brief Checks whether all values of a transition
         *          are greater than or equal to zero
         * @return true iff the transition is valid
         */
        bool isValid()
        {
            return (duration >= 0)
                   && (start >= 0)
                   && (it >= 0);
        }
    } GuiTransition;

    /**
     * @class TransitionController
     * @brief Subcontroller which handles all user interaction with the transition
     *          tab in the GUI, communicates with other controllers via signals
     *          and slots
     */
    class TransitionController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new TransitionController and assigns a TransitionTab to handle
         * @param guiTransitionTab Pointer to the TransitionTab whose user interaction
         *                         is handled by the TransitionController
         */
        TransitionController(TransitionTabPtr guiTransitionTab);

        /**
         * @brief Getter for the TransitionTab pointer to guiTransitionTab
         * @return A Pointer to the guiTransitionTab
         */
        TransitionTabPtr getGuiTransitionTab();

        /**
         * @brief Setter for the TransitionTab pointer to guiTransitionTab
         * @param guiTransitionTab Pointer to the TransitionTab whose user interaction
         *                         is handled by the TransitionController
         */
        void setGuiTransitionTab(TransitionTabPtr guiTransitionTab);

    public slots:
        /*
         * Changed parameters from QString name, QVariant userData
         * to int start, int end, double duration
         * to set the QVariant data within this method
         */
        /**
         * @brief Adds a new transition to the list widget
         * @param index Index of the transition
         * @param duration Duration of the transition
         * @param start Start time of the transition
         * @param interpolation Index of the interpolation
         */
        void addTransition(int index, double duration, double start, int interpolation);

        /**
         * @brief Removes the transition at a given index from the list widget
         * @param index Index of the transition
         */
        void removeTransition(int index);

        /**
         * @brief  Updates the currently selected transition
         * @param index Index of the currently selected transition in the list widget
         */
        void updateSelectedTransition(int index);

        /**
         * @brief Connected with signals from other controllers, sets all
         *        values of the transition at a given index
         * @param transition Index of the transition
         * @param duration Duration of the transition
         * @param start Start time of the transition
         * @param it Index of the interpolation of the transition
         */
        void setTransitionData(int index, double duration, double start, int it);

        /**
         * @brief Retranslates the guiTransitionTab
         */
        void retranslateGui();

        /**
         * @brief Removes all items of the transition list
         */
        void clearTransitionList();

    private slots:
        /**
         * @brief Updates the currently selected transition
         * @param item Currently selected transition in the list widget
         */
        void updateSelectedTransition(QListWidgetItem* item);

        /**
         * @brief Sets the duration of the currently selected transition
         */
        void setDurationValue();

        /**
         * @brief Sets the interpolation of the currently selected transition
         * @param index Index of the interpolation
         */
        void setInterpolation(int index);

        /**
         * @brief Enables or disables the duration, interpolation and list widget
         * @param enable Determines whether to enable or disable the duration, interpolation and list widget
         */
        void enableAll(bool enable);

    signals:
        /**
         * @brief Notifies other controllers about changes of the duration of
         *          the given transition
         * @param index Index of the transition
         * @param duration New duration value of the transition
         */
        void changedDuration(int transition, double duration);

        /**
         * @brief Notifies other controllers about changes of the interpolation
         *          type of the given transition
         * @param index Index of the transition
         * @param it New interpolation index of the transition
         */
        void changedInterpolation(int index, int it);


        void selectedTransitionChanged(int index);

    private:
        TransitionTabPtr guiTransitionTab;

        /**
         * @brief Auxiliary method checking whether a list widget contains certain data,
         *          namely a certain GuiTransition
         * @param list QListWidget to check
         * @param transition GuiTransition to check for
         * @return true, if the list contains the data otherwise return false
         */
        bool contains(QListWidget* list, GuiTransition transition);

        /**
         * @brief Initializes the interpolation combo box
         */
        void initInterpolationComboBox();

        //@Max eingefügt von @Tim dadurch: Reihenfolge Item == Text Item
        void changeTextListWidgetItems();
    };

    typedef std::shared_ptr<TransitionController> TransitionControllerPtr;
}

Q_DECLARE_METATYPE(armarx::GuiTransition)

#endif // TRANSITIONCONTROLLER_H
