#ifndef IMPORTDIALOGCONTROLLER_H
#define IMPORTDIALOGCONTROLLER_H
#include "AbstractController.h"
#include "../Model/DesignerTrajectory.h"
#include "../ImportExport/MMMImporter.h"

namespace armarx
{
    /**
     * @class ImportDialogController
     * @brief Subcontroller which handles all user interaction with the import
     *          dialog in the GUI, communicates with other controllers
     *          via signals and slots
     */
    class ImportDialogController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new ImportDialogController and assigns a designer
         *          importer to handle
         */
        ImportDialogController();

    public slots:
        /**
         * @brief Opens an import dialog
         */
        void open();

        /**
         * @brief Set the enviroment
         * @param The new enviroment
         */
        void environmentChanged(EnvironmentPtr environment);

    signals:
        /**
         * @brief Notifies other controllers about the import of a trajectory
         */
        void import(DesignerTrajectoryPtr trajectory);
    private:
        EnvironmentPtr environment;

        void helpExceptionMessageBox(std::string errorMessage);
    };

    typedef std::shared_ptr<ImportDialogController> ImportDialogControllerPtr;
}

#endif // IMPORTDIALOGCONTROLLER_H
