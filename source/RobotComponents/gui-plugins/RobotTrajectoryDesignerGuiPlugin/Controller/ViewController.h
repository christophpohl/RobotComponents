/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::ViewController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H
#include "AbstractController.h"
#include "../View/Perspectives.h"
#include "../Util/WheelEventFilter.h"
#include <QMouseEvent>
#include <QComboBox>
#include <QPushButton>
#include <QCheckBox>

namespace armarx
{
    /**
     * @class ViewController
     * @brief Subcontroller which handles all user interaction with the view
     *          selection area in the GUI, communicates with other controllers
     *          via signals and slots
     */
    class ViewController : public AbstractController
    {
        Q_OBJECT

    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new ViewController and assigns a Perspectives widget
         *         to handle
         * @param guiPerspectives Pointer to the Perspectives whose user interaction
         *                         is handled by the ViewController
         */
        ViewController(PerspectivesPtr guiPerspectives);

        /**
         * @brief Getter for the Perspectives pointer to guiPerspectives
         * @return A Pointer to the guiPerspectives
         */
        PerspectivesPtr getGuiPerspectives();

        /**
         * @brief Setter for the Perspectives pointer to guiPerspectives
         * @param guiPerspectives Pointer to the Perspectives whose user interaction
         *                          is handled by the ViewController
         */
        void setGuiPerspectives(PerspectivesPtr guiPerspectives);

    public slots:
        /**
         * @brief Sets the perspective on the displayed robot model
         * @param index Index of the perspective
         */
        void setViewPerspective(int index);

        /**
         * @brief Retranslates the guiPerspectives
         */
        void retranslateGui();

    private slots:
        void addViewSlot();
        void removeViewSlot();

    signals:
        /**
         * @brief Notifies other controllers about changes of the perspective
         * @param perspective Index of the current perspective
         */
        void changedPerspective(int perspective);

        /**
         * @brief Notifies RobotVisualizationController to add a view
         */
        void addView();

        /**
         * @brief Notifies RobotVisualizationController to delete a view
         */
        void removeView();

    private:
        PerspectivesPtr guiPerspectives;
        int numberViews;

        /**
         * @brief Initializes the perspectives combo box
         */
        void initPerspectivesCombobox();

        void enableAddRemoveViewButton();
    };

    typedef std::shared_ptr<ViewController> ViewControllerPtr;
}

#endif // VIEWCONTROLLER_H
