/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TCPInformationController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef TCPINFORMATIONCONTROLLER_H
#define TCPINFORMATIONCONTROLLER_H
#include "AbstractController.h"
#include "../View/TCPInformationTab.h"
#include <memory>
#include <Eigen/Core>

#include <QLayoutItem>
#include <QLineEdit>

namespace armarx
{

    /**
     * @class TCPInformationController
     * @brief Subcontroller which handles all user interaction with the tcp
     *          information tab in the GUI, communicates with other controllers
     *          via signals and slots
     */
    class TCPInformationController : public AbstractController
    {
        Q_OBJECT
    public:
        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new TCPInformationController and assigns a TCPInformationTab
         *          to handle
         * @param guiTCPInformation Pointer to the TCPInformationTab whose user
         *                          interaction is handled by the
         *                          TCPInformationController
         */
        TCPInformationController(TCPInformationPtr guiTCPInformation);

        /**
         * @brief Getter for the TCPInformationTab pointer to guiTCPInformation
         * @return A Pointer to the guiTCPInformation
         */
        TCPInformationPtr getGuiTCPInformation();

        /**
         * @brief Setter for the TCPInformationTab pointer to guiTCPInformation
         * @param guiTCPInformation Pointer to the TCPInformationTab whose user interaction
         *                          is handled by the TCPInformationController
         */
        void setGuiTCPInformation(TCPInformationPtr guiTCPInformation);

    public slots:
        /**
         * @brief Sets all values of the current pose
         * @param pose A list of strings containing all values of the current
         *              pose
         */
        void setCurrentPose(Eigen::Matrix4f pose);

        /**
         * @brief Sets all values of the desired pose
         * @param pose A list of strings containing all values of the desired
         *              pose
         */
        void setDesiredPose(Eigen::Matrix4f pose);

        /**
         * @brief setReachable set the label isReachable to "reachable" if true and "not reachable" if false
         * @param reachable
         */
        void setReachable(bool reachable);

        /**
         * @brief Resets the labels of current and desired pose to zeroes
         */
        void clearPoseLabels();

        /**
         * @brief Retranslates the guiTCPInformation
         */
        void retranslateGui();

    private:
        TCPInformationPtr guiTCPInformation;

        /**
         * @brief Sets the text on all line edits of a given grid layout from
         *          a given line edit.
         * @param gridLayout Grid layout containing line edits to access
         * @param pose List of strings containing data of a pose
         */
        void setLineEditText(QGridLayout* gridLayout, Eigen::Matrix4f pose);
    };

    typedef std::shared_ptr<TCPInformationController> TCPInformationControllerPtr;
}


#endif // TCPINFORMATIONCONTROLLER_H
