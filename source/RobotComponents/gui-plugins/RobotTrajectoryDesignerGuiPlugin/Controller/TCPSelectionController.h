/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Controller::TCPSelectionController
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef TCPSELECTIONCONTROLLER_H
#define TCPSELECTIONCONTROLLER_H
#include "AbstractController.h"
#include "../Util/WheelEventFilter.h"

#include <QComboBox>

namespace armarx
{
    /**
     * @class TCPSelectionController
     * @brief Subcontroller which handles all user interaction with the active TCP
     *          selection in the GUI, communicates with other controllers via
     *          signals and slots
     */
    class TCPSelectionController : public AbstractController
    {
        Q_OBJECT

    public:

        /**
         * @brief @see AbstractController
         */
        void onInitComponent();

        /**
         * @brief @see AbstractController
         */
        void onConnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onDisconnectComponent();

        /**
         * @brief @see AbstractController
         */
        void onExitComponent();

        /**
         * @brief Creates a new TCPSelectionController and assigns a QWidget to handle
         * @param currentTCPCombobox Pointer to the QComboBox whose user interaction
         *                              is handled by the TCPSelectionController
         */
        TCPSelectionController(QComboBox* currentTCPCombobox);

        /**
         * @brief Getter for the QComboBox pointer to currentTCPCombobox
         * @return A Pointer to the currentTCPCombobox
         */
        QComboBox* getCurrentTCPCombobox();

        /**
         * @brief Setter for the QComboBox pointer to currentTCPCombobox
         * @param currentTCPCombobox Pointer to the QComboBox whose user interaction
         *                              is handled by the TCPSelectionController
         */
        void setCurrentTCPCombobox(QComboBox* currentTCPCombobox);

    public slots:
        /**
         * @brief Updates the currently displayed trajectory of the corresponding TCP
         * @param trajectory String identifier of the trajectory
         */
        void updateSelectedTCP(QString trajectory);

        /**
         * @brief Adds a new trajectory to the combobox of available trajectories
         * @param trajectory String identifier of the trajectory
         */
        void addTrajectory(QString trajectory);

        /**
         * @brief Removes the given trajectory from the TCP selection combo box
         * @param trajectory String identifier of the trajectory
         */
        void removeTrajectory(QString trajectory);

        /**
         * @brief Enables or disables the selected tcp combobox
         * @param enable Determines whether to enable or disable the selected tcp combobox
         */
        void enableSelectedTCP(bool enable);

        /**
         * @brief Removes all trajectorie is tcpComboBox
         */
        void removeAllTrajectories();

    private slots:
        /**
         * @brief Updates the currently displayed trajectory of the corresponding TCP
         * @param index Index of the trajectory
         */
        void updateSelectedTCP(int index);


    signals:
        /**
         * @brief Notifies other controllers about changes of the currently displayed trajectory
         * @param index Index of the trajectory
         */
        void changedSelectedTCP(QString trajectory);

    private:
        QComboBox* currentTCPCombobox;
    };

    typedef std::shared_ptr<TCPSelectionController> TCPSelectionControllerPtr;
}

#endif // TCPSELECTIONCONTROLLER_H
