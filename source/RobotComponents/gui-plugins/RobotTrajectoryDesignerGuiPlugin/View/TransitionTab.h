/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::View::TransitionTab
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef TRANSITIONTAB_H
#define TRANSITIONTAB_H

#include "ui_TransitionTab.h"
#include <QWidget>
#include <QListWidget>
#include <memory>

namespace Ui
{
    class TransitionTab;
}

class TransitionTab : public QWidget
{
    Q_OBJECT

public:
    explicit TransitionTab(QWidget* parent = 0);
    ~TransitionTab();

    Ui::TransitionTab* getTransitionTab();
    void setTransitionTab(Ui::TransitionTab* transitionTab);

private:
    Ui::TransitionTab* transitionTab;
};

typedef std::shared_ptr<TransitionTab> TransitionTabPtr;

#endif // TRANSITIONTAB_H
