/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    RobotTrajectoryDesigner::gui-plugins::Util::WheelEventFilter
* \author     Max Beddies
* \date       2018
* \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef WHEELEVENTFILTER_H
#define WHEELEVENTFILTER_H

#include <QObject>
#include <QEvent>
#include <QComboBox>

namespace armarx
{
    class WheelEventFilter : public QObject
    {
        Q_OBJECT
    public:
        WheelEventFilter(QObject* parent = 0);

        /**
         * @brief Event filter which filters wheel events from combo boxes
         * @param obj Watched object (combo box)
         * @param e Event to filter
         * @return true, iff the event is a wheel event
         */
        bool eventFilter(QObject* obj, QEvent* e);
    };
}

#endif // WHEELEVENTFILTER_H
