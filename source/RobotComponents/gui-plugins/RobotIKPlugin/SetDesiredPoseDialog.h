#ifndef SETDESIREDPOSEDIALOG_H
#define SETDESIREDPOSEDIALOG_H

#include <QDialog>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

using namespace armarx;

namespace Ui
{
    class SetDesiredPoseDialog;
}

class SetDesiredPoseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetDesiredPoseDialog(QWidget* parent = 0);
    ~SetDesiredPoseDialog();

    FramedPosePtr getDesiredPose();

private:
    Ui::SetDesiredPoseDialog* ui;

    bool stringToJSON(std::string string, JsonObjectPtr& result) const;

    FramedPosePtr result;

private slots:
    void checkJSON();
    void formatInput();
    void parseInputAndSetPose();
};

#endif // SETDESIREDPOSEDIALOG_H
