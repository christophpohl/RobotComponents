/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::PathPlanner
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
* @date       2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
#include "PathPlanner.h"

namespace armarx
{
    /**
     * @defgroup Component-AStarPathPlanner AStarPathPlanner
     * @brief The AStarPathPlanner provides path planning using the A* algorithm.
     *
     *
     *
     * With this component collision-free paths can be planned using the \ref armarx::PathPlanner interface.
     * You can specify several parameters and the envrionment via convenient methods.
     * The paths are encoded as sequence of 3D points consisting of X/Y positions (mm) and an orientation (rad) arounf the z axis.
     * An exemplary path that has been planned with this component can be seen below.
     *
     * \image html PathPlanning_small.png "A collision-free path for Armar3."
     *
     *
     *
     *
     * @ingroup RobotComponents-Components
     */

    /**
     * @ingroup Component-AStarPathPlanner
     * @brief The AStarPathPlanner class
     */
    class AStarPathPlanner:
        virtual public PathPlanner,
        virtual public AStarPathPlannerBase
    {
    public:
        AStarPathPlanner();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "AStarPathPlanner";
        }
        //slice impl from PathPlannerBase
        ::armarx::Vector3BaseList getPath(const ::armarx::Vector3BasePtr& from, const ::armarx::Vector3BasePtr& to, const ::Ice::Current& = ::Ice::Current()) const override;

        //slice impl from PathPlannerBase
        void setTranslationtStepSize(::Ice::Float step, const ::Ice::Current& = ::Ice::Current()) override;
        void setRotationStepSize(::Ice::Float step, const ::Ice::Current& = ::Ice::Current()) override;

        void setBoundingXRange(const ::armarx::FloatRange& range, const ::Ice::Current& = ::Ice::Current()) override;

        void setBoundingYRange(const ::armarx::FloatRange& range, const ::Ice::Current& = ::Ice::Current()) override;

        void setNeighbourSteps(const ::armarx::Vector3IBaseList& neighbours, const ::Ice::Current& = ::Ice::Current()) override;

        void setRotationFactorForHeuristic(::Ice::Float factor, const ::Ice::Current& = ::Ice::Current()) override;

        void setDistanceFactorForHeuristic(::Ice::Float factor, const ::Ice::Current& = ::Ice::Current()) override;

        /**
         * @brief The heuristic used for A*.
         * @param from From
         * @param to To
         * @return distanceFactorForHeuristic * distanceDelta + rotationFactorForHeuristic * rotationDelta
         */
        float heuristic(const armarx::Vector3& from, const armarx::Vector3& to) const;

        float heuristic(const armarx::Vector3IBase& from, const armarx::Vector3IBase& to) const;

    protected:
        /**
         * @brief The step size in degree.
         */
        int stepSizeDeg;
    };

    typedef ::IceInternal::Handle< ::armarx::AStarPathPlanner> AStarPathPlannerPtr;
}
