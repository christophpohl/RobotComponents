/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::GraspSelectionManager
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotComponents/interface/components/GraspingManager/GraspSelectionManagerInterface.h>

namespace armarx
{

    class GraspSelectionManagerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        GraspSelectionManagerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            //            defineOptionalProperty<float>("AskHumanThreshold", 0.7f, "Confidence threshold below which the human will be asked for confirmation of a replacement");
        }
    };


    /*!
     * \brief The GraspSelectionManager class
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT GraspSelectionManager :
        virtual public GraspSelectionManagerInterface,
        virtual public Component
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "GraspSelectionManager";
        }

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(
                       new GraspSelectionManagerPropertyDefinitions(getConfigIdentifier()));
        }

        void onInitComponent() override;
        void onConnectComponent() override;

        GeneratedGraspList filterGrasps(const GeneratedGraspList& grasps, const Ice::Current& = Ice::Current()) override;
        void registerAsGraspSelectionCriterion(const GraspSelectionCriterionInterfacePrx& criterion, const Ice::Current& = Ice::Current()) override;
        GraspSelectionCriterionInterfaceList getRegisteredGraspSelectionCriteria(const Ice::Current&) override;
        //FeedbackPublisherInterface

        std::vector<GraspSelectionCriterionInterfacePrx> criteria;
    };
}

