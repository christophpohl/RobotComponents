
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore PlannedMotionProvider)
 
armarx_add_test(PlannedMotionProviderTest PlannedMotionProviderTest.cpp "${LIBS}")