/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::PlannedMotionProvider
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/interface/components/PlannedMotionProviderInterface.h>
#include <RobotComponents/components/MotionPlanning/MotionPlanningObjectFactories.h>

namespace armarx
{
    /**
     * @class PlannedMotionProviderPropertyDefinitions
     * @brief
     */
    class PlannedMotionProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PlannedMotionProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-PlannedMotionProvider PlannedMotionProvider
     * @ingroup RobotComponents-Components
     * A description of the component PlannedMotionProvider.
     *
     * @class PlannedMotionProvider
     * @ingroup Component-PlannedMotionProvider
     * @brief Brief description of class PlannedMotionProvider.
     *
     * Detailed description of class PlannedMotionProvider.
     */
    class PlannedMotionProvider :
        virtual public armarx::Component,
        virtual public armarx::PlannedMotionProviderInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PlannedMotionProvider";
        }

        GraspingTrajectory planMotion(const SimoxCSpaceBasePtr& cSpaceBase, const SimoxCSpaceBasePtr& cspacePlatformBase, const MotionPlanningData& mpd, const Ice::Current& c = Ice::Current()) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        std::vector<RemoteHandle<MotionPlanningTaskControlInterfacePrx>> planningTasks;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
        MotionPlanningServerInterfacePrx mps;
    };
}

