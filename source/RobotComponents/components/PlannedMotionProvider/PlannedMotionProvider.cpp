/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::PlannedMotionProvider
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlannedMotionProvider.h"
#include <VirtualRobot/Nodes/RobotNode.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotComponents/components/MotionPlanning/Tasks/RRTConnect/Task.h>
//#include <RobotComponents/components/MotionPlanning/Tasks/BiRRT/Task.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.h>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
#include <RobotComponents/components/MotionPlanning/CSpace/ScaledCSpace.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>


namespace armarx
{
    GraspingTrajectory PlannedMotionProvider::planMotion(const SimoxCSpaceBasePtr& cSpaceBase, const SimoxCSpaceBasePtr& cspacePlatformBase, const MotionPlanningData& mpd, const Ice::Current& c)
    {
        SimoxCSpacePtr cSpace = SimoxCSpacePtr::dynamicCast(cSpaceBase);
        cSpace->setStationaryObjectMargin(70);
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cSpace);
        SimoxCSpacePtr cSpacePlatform = SimoxCSpacePtr::dynamicCast(cspacePlatformBase);
        cSpacePlatform->setStationaryObjectMargin(100);
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cSpacePlatform);

        Ice::FloatSeq cspaceScaling;
        for (VirtualRobot::RobotNodePtr node : localRobot->getRobotNodeSet(mpd.rnsToUse)->getAllRobotNodes())
        {
            cspaceScaling.push_back(node->isTranslationalJoint() ? 0.001f : 1.0f);
        }
        ScaledCSpacePtr scaledJointCSpace = new ScaledCSpace(cSpace, cspaceScaling);
        ARMARX_IMPORTANT << "Robot position: " << VAROUT(mpd.globalPoseStart->output()) << VAROUT(mpd.globalPoseGoal->output());

        armarx::VectorXf startCfg = cSpace->jointMapToVector(mpd.configStart);
        armarx::VectorXf goalCfg = cSpace->jointMapToVector(mpd.configGoal);
        scaledJointCSpace->scaleConfig(startCfg);
        scaledJointCSpace->scaleConfig(goalCfg);


        float dcdStep = 0.01f;
        float dcdStepPlatform = 0.1;
        int maxPostProcessingSteps = 50;
        ARMARX_VERBOSE << VAROUT(startCfg) << VAROUT(goalCfg);

        ARMARX_VERBOSE << "Planning joint trajectory using RRT";
        MotionPlanningTaskBasePtr rrt = new BiRRTTask {scaledJointCSpace, startCfg, goalCfg, getDefaultName() + ".jointbirrt" + IceUtil::generateUUID(), 60, dcdStep};
        //        MotionPlanningTaskBasePtr rrt = new RRTConnectTask(scaledJointCSpace, startCfg, goalCfg, getDefaultName() + "JointRRT" + IceUtil::generateUUID(), 10, dcdStep, coreCount);
        //        MotionPlanningTaskBasePtr rrt = new birrt::Task(cSpace, startCfg, goalCfg, getDefaultName() + "JointBiRRT" + IceUtil::generateUUID(), dcdStep);

        RandomShortcutPostprocessorTaskHandle rsppHandle = mps->enqueueTask(new RandomShortcutPostprocessorTask(rrt, "JointRRTSmoothing" + IceUtil::generateUUID(), 1, dcdStep, maxPostProcessingSteps));
        planningTasks.push_back(rsppHandle);

        rsppHandle->waitForFinishedRunning();
        ARMARX_IMPORTANT << "joint trajectory planning took " << IceUtil::Time::microSeconds(rsppHandle->getRunningTime()).toMilliSecondsDouble() << " ms";

        if (rsppHandle->getTaskStatus() == armarx::TaskStatus::ePlanningFailed)
        {
            throw RuntimeError("Arm Motion Planning failed!");
        }

        Eigen::Vector3f rpy, rpyAgent;
        VirtualRobot::MathTools::eigen4f2rpy(PosePtr::dynamicCast(mpd.globalPoseStart)->toEigen(), rpy);
        VirtualRobot::MathTools::eigen4f2rpy(PosePtr::dynamicCast(cSpacePlatform->getAgent().agentPose)->toEigen(), rpyAgent);
        armarx::VectorXf startPos {mpd.globalPoseStart->position->x - cSpacePlatform->getAgent().agentPose->position->x,
                                   mpd.globalPoseStart->position->y - cSpacePlatform->getAgent().agentPose->position->y,
                                   math::MathUtils::angleModPI(rpy(2) - rpyAgent(2))
                                  };

        Eigen::Vector2f localStartPos = Eigen::Rotation2Df(-rpyAgent(2)) * Eigen::Vector2f(startPos.at(0), startPos.at(1));
        startPos.at(0) = localStartPos(0);
        startPos.at(1) = localStartPos(1);
        VirtualRobot::MathTools::eigen4f2rpy(PosePtr::dynamicCast(mpd.globalPoseGoal)->toEigen(), rpy);
        armarx::VectorXf goalPos {mpd.globalPoseGoal->position->x - cSpacePlatform->getAgent().agentPose->position->x,
                                  mpd.globalPoseGoal->position->y - cSpacePlatform->getAgent().agentPose->position->y,
                                  math::MathUtils::angleModPI(rpy(2) - rpyAgent(2))
                                 };
        Eigen::Vector2f localGoalPos = Eigen::Rotation2Df(-rpyAgent(2)) * Eigen::Vector2f(goalPos.at(0), goalPos.at(1));
        goalPos.at(0) = localGoalPos(0);
        goalPos.at(1) = localGoalPos(1);

        ARMARX_INFO << VAROUT(startPos) << VAROUT(goalPos);

        ScaledCSpacePtr scaledPlatformCSpace = new ScaledCSpace(cSpacePlatform, {0.001, 0.001, 1});
        scaledPlatformCSpace->scaleConfig(startPos);
        scaledPlatformCSpace->scaleConfig(goalPos);

        //    BiRRTTask;

        MotionPlanningTaskBasePtr taskPlatformRRT = new BiRRTTask {scaledPlatformCSpace, startPos, goalPos, getDefaultName() + ".birrt" + IceUtil::generateUUID(), 60, dcdStepPlatform};

        //        MotionPlanningTaskBasePtr taskPlatformRRT = new armarx::RRTConnectTask {scaledPlatformCSpace, startPos, goalPos, getDefaultName() + "RRT" + IceUtil::generateUUID(), 60, platformDcdStep, coreCount};
        RandomShortcutPostprocessorTaskHandle rsppHandlePlatform = mps->enqueueTask(new RandomShortcutPostprocessorTask(taskPlatformRRT, "PlatformRRTSmoothing" + IceUtil::generateUUID(), 1, dcdStepPlatform, maxPostProcessingSteps));
        planningTasks.push_back(rsppHandlePlatform);

        rsppHandlePlatform->waitForFinishedRunning();
        ARMARX_IMPORTANT << "platform trajectory planning took " << IceUtil::Time::microSeconds(rsppHandlePlatform->getRunningTime()).toMilliSecondsDouble() << " ms";

        if (rsppHandlePlatform->getTaskStatus() == armarx::TaskStatus::ePlanningFailed)
        {
            throw RuntimeError("Platform Motion Planning failed!");
        }

        ARMARX_INFO << "Joint Motion Planning " << ((bool)((int)(rsppHandle->getTaskStatus()) == (int)(armarx::TaskStatus::ePlanningFailed)) ? "failed" : "succeeded");
        ARMARX_INFO << "Joint Motion Planning status: " << rsppHandle->getTaskStatus();

        ARMARX_INFO << "RRTConnectTask Planning " << ((bool)(rsppHandlePlatform->getTaskStatus() == armarx::TaskStatus::ePlanningFailed) ? "failed" : "succeeded");
        ARMARX_INFO << "RRTConnectTask Planning status: " << rsppHandlePlatform->getTaskStatus();


        auto jointTrajectoryPath = rsppHandle->getPath();
        auto posTrajectoryPath = rsppHandlePlatform->getPath();
        scaledJointCSpace->unscalePath(jointTrajectoryPath);
        scaledPlatformCSpace->unscalePath(posTrajectoryPath);

        auto transformToGlobal = [&](armarx::VectorXf & pos2D)
        {
            Eigen::Vector2f globalPos = Eigen::Rotation2Df(rpyAgent(2)) * Eigen::Vector2f(pos2D.at(0), pos2D.at(1));
            pos2D.at(0) = globalPos[0] + cSpacePlatform->getAgent().agentPose->position->x;
            pos2D.at(1) = globalPos[1] + cSpacePlatform->getAgent().agentPose->position->y;
            pos2D.at(2) = math::MathUtils::angleModPI(pos2D.at(2) + rpyAgent(2));

        };
        for (auto& e : posTrajectoryPath.nodes)
        {
            transformToGlobal(e);
        }

        // retransform platform path to global poses

        //    TrajectoryPtr posTrajectory(new Trajectory(posTrajectoryPath.nodes, Ice::DoubleSeq {}, {"x", "y", "alpha"}));
        //TODO check if solution found
        return {cSpacePlatform->pathToTrajectory(posTrajectoryPath),
                cSpace->pathToTrajectory(jointTrajectoryPath), mpd.rnsToUse, mpd.endeffector,
                mpd.grasp
               };
    }

    void PlannedMotionProvider::onInitComponent()
    {
        usingProxy("MotionPlanningServer");
        usingProxy("RobotStateComponent");
    }


    void PlannedMotionProvider::onConnectComponent()
    {
        assignProxy(mps, "MotionPlanningServer");
        assignProxy(robotStateComponent, "RobotStateComponent");
        localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
    }


    void PlannedMotionProvider::onDisconnectComponent()
    {
        planningTasks.clear();
    }


    void PlannedMotionProvider::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr PlannedMotionProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new PlannedMotionProviderPropertyDefinitions(
                getConfigIdentifier()));
    }
}

