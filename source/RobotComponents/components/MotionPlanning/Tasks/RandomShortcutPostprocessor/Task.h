/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <mutex>
#include <chrono>
#include <atomic>
#include <random>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
#include <ArmarXCore/interface/core/RemoteObjectNode.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.h>
#include "../../util/Metrics.h"

#include "../MotionPlanningTask.h"

namespace armarx
{
    namespace rngshortcut
    {
        class Task;
        typedef IceInternal::Handle<Task> TaskPtr;

        class Task:
            public virtual PostprocessingMotionPlanningTask,
            public virtual TaskBase,
            public virtual MotionPlanningMultiPathWithCostTaskCI
        {
        public:
            Task(
                MotionPlanningTaskBasePtr previousStep,
                const std::string& taskName = "RandomShortcutPostprocessorTask",
                Ice::Long maxTimeForPostprocessingInSeconds = 6000,
                Ice::Float dcdStep = 0.01,
                Ice::Long maxTries = 10000,
                Ice::Float minShortcutImprovementRatio = 0.1,
                Ice::Float minPathImprovementRatio = 0.01
            );


            PathWithCost getPathWithCost(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getPathWithCost();
            }
            Path getNthPath(Ice::Long n, const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getNthPath(n);
            }
            Path getPath(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningWithCostTaskCI::getPath();
            }

            ~Task() override = default;

            //PlanningControlInterface
            /**
            * @brief Aborts planning.
            */
            void abortTask(const Ice::Current& = Ice::Current()) override;

            //PlanningTaskBase
            /**
            * @brief Runs the task.
            * @param remoteNodes The list of \ref RemoteObjectNodeInterfacePrx used to distribute work to computers.
            */
            void run(const RemoteObjectNodePrxList& nodes, const Ice::Current& = Ice::Current()) override;

            Ice::Long getMaximalPlanningTimeInSeconds(const Ice::Current& = Ice::Current()) const override
            {
                return previousStep->getMaximalPlanningTimeInSeconds();
            }

            Ice::Long getPathCount(const Ice::Current& = Ice::Current()) const override;
            PathWithCost getBestPath(const Ice::Current& = Ice::Current()) const override;
            PathWithCost getNthPathWithCost(Ice::Long n, const Ice::Current& = Ice::Current()) const override;
            PathWithCostSeq getAllPathsWithCost(const Ice::Current& = Ice::Current()) const override;

            bool isPathCollisionFree(const VectorXf& from, const VectorXf& to);

            std::pair<float, float> calcOffsets(float length, std::mt19937& gen);

        protected:
            /**
            * @brief Ctor used by object factories.
            */
            Task() = default;

            mutable std::mutex mtx;
            Path postprocessedSolution;
            PathWithCostSeq paths;
            std::atomic_bool taskIsAborted {false};
        private:
            template<class Base, class Derived> friend class ::armarx::GenericFactory;
        };
    }
    using RandomShortcutPostprocessorTask = rngshortcut::Task;
    using RandomShortcutPostprocessorTaskPtr = IceUtil::Handle<RandomShortcutPostprocessorTask>;
    using RandomShortcutPostprocessorTaskHandle = RemoteHandle<MotionPlanningMultiPathWithCostTaskControlInterfacePrx>;
}
