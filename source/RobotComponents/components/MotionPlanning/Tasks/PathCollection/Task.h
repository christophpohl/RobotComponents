/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <mutex>
#include <chrono>
#include<atomic>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
#include <ArmarXCore/interface/core/RemoteObjectNode.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/PathCollection/Task.h>

#include "../MotionPlanningTask.h"
#include "../../util/Metrics.h"

namespace armarx
{
    namespace pathcol
    {
        class Task;
        typedef IceInternal::Handle<Task> TaskPtr;

        class Task:
            public virtual MotionPlanningTask,
            public virtual TaskBase,
            public virtual MotionPlanningMultiPathWithCostTaskCI
        {
        public:
            Task(const std::string& taskName = "PathCollection")
            {
                this->taskName = taskName;
            }

            ~Task() override = default;


            PathWithCost getPathWithCost(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getPathWithCost();
            }
            Path getNthPath(Ice::Long n, const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getNthPath(n);
            }
            Path getPath(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningWithCostTaskCI::getPath();
            }

            void abortTask(const Ice::Current& = Ice::Current()) override
            {
            }

            CSpaceBasePtr getCSpace(const Ice::Current& = ::Ice::Current()) const override
            {
                return cspace;
            }

            /**
            * @brief Runs the task.
            * @param remoteNodes The list of \ref RemoteObjectNodeInterfacePrx used to distribute work to computers.
            */
            void run(const RemoteObjectNodePrxList&, const Ice::Current& = Ice::Current()) override;

            Ice::Long getMaximalPlanningTimeInSeconds(const Ice::Current& = Ice::Current()) const override
            {
                return maximalPlanningTimeInSeconds;
            }

            Ice::Long getPathCount(const Ice::Current& = Ice::Current()) const override
            {
                return paths.size();
            }
            PathWithCost getBestPath(const Ice::Current& = Ice::Current()) const override
            {
                return getNthPathWithCost(0);
            }
            PathWithCost getNthPathWithCost(Ice::Long n, const Ice::Current& = Ice::Current()) const override
            {
                if (static_cast<std::size_t>(n) < paths.size())
                {
                    return paths.at(n);
                }
                return {{}, std::numeric_limits<float>::infinity(), "Path_" + to_string(n)};
            }
            PathWithCostSeq getAllPathsWithCost(const Ice::Current& = Ice::Current()) const override
            {
                return paths;
            }

            void addPath(Path p)
            {
                float len = 0;
                for (std::size_t i = 0; i + 1 < p.nodes.size(); ++i)
                {
                    len += euclideanDistance(p.nodes.at(i).begin(), p.nodes.at(i).end(), p.nodes.at(i + 1).begin());
                }
                addPath(PathWithCost {std::move(p.nodes), len, std::move(p.pathName)});
            }
            void addPath(PathWithCost p)
            {
                paths.emplace_back(std::move(p));
            }
        };
    }
    using PathCollection = pathcol::Task;
    using PathCollectionPtr = IceUtil::Handle<PathCollection>;
    using PathCollectionHandle = RemoteHandle<MotionPlanningMultiPathWithCostTaskControlInterfacePrx>;
}
