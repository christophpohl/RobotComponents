/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <chrono>
#include <numeric>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <RobotComponents/interface/components/MotionPlanning/ResourceRequestStrategies/ComputingPowerRequestStrategy.h>

namespace armarx
{
    namespace cprs
    {
        /**
         * @brief Implementation of ComputingPowerRequestStrategyBase
         * @see ComputingPowerRequestStrategyBase
         */
        class ComputingPowerRequestStrategy:
            virtual public ComputingPowerRequestStrategyBase
        {
        public:
            /**
             * @brief Dtor
             */
            ~ComputingPowerRequestStrategy() override = default;

            /**
             * @brief Default implementation. Does nothing.
             * @see ComputingPowerRequestStrategyBase::setCurrentStateAsInitialState
             */
            void setCurrentStateAsInitialState(const ::Ice::Current& = ::Ice::Current()) override
            {
            }

            /**
             * @brief Default implementation. Does nothing.
             * @see ComputingPowerRequestStrategyBase::updateNodeCount
             */
            void updateNodeCount(Ice::Long, const Ice::Current& = Ice::Current()) override
            {
            }
            /**
             * @brief Default implementation. Does nothing.
             * @see ComputingPowerRequestStrategyBase::updateTaskStatus
             */
            void updateTaskStatus(armarx::TaskStatus::Status, const Ice::Current& = Ice::Current()) override
            {
            }

            /**
             * @brief Default implementation. Does nothing.
             * @see ComputingPowerRequestStrategyBase::allocatedComputingPower
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override
            {
            }

            /**
             * @brief Default implementation. Does nothing.
             * @see ComputingPowerRequestStrategyBase::updateNodeCreations
             */
            void updateNodeCreations(Ice::Long, Ice::Long, const Ice::Current& = Ice::Current()) override
            {
            }

            /**
             * @brief Returns whether new computing power should be allocated.
             * Pure virtual.
             * @see ComputingPowerRequestStrategyBase::shouldAllocateComputingPower
             * @return Whether new computing power should be allocated.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override = 0;
        };
        typedef IceInternal::Handle<ComputingPowerRequestStrategy> ComputingPowerRequestStrategyPtr;

        /**
         * @brief Implementation of CompoundedRequestStrategyBase
         * @see CompoundedRequestStrategyBase
         */
        class CompoundedRequestStrategy:
            virtual public CompoundedRequestStrategyBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param requestStrategies The strategies forming this compound.
             */
            CompoundedRequestStrategy(const ComputingPowerRequestStrategyBaseList& requestStrategies): CompoundedRequestStrategyBase(requestStrategies) {}

            /**
             * @brief Calls this function for all compounded strategies.
             * @see ComputingPowerRequestStrategyBase::setCurrentStateAsInitialState
             */
            void setCurrentStateAsInitialState(const ::Ice::Current& = ::Ice::Current()) override;
            /**
             * @brief Calls this function for all compounded strategies.
             * @see ComputingPowerRequestStrategyBase::updateNodeCount
             * @param count The new node count
             */
            void updateNodeCount(Ice::Long count, const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Calls this function for all compounded strategies.
             * @see ComputingPowerRequestStrategyBase::updateTaskStatus
             * @param newStatus The new status
             */
            void updateTaskStatus(armarx::TaskStatus::Status newStatus, const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Calls this function for all compounded strategies.
             * @see ComputingPowerRequestStrategyBase::allocatedComputingPower
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Passes this call to all sub strategies.
             * @see ComputingPowerRequestStrategyBase::updateNodeCreations
             * @param nodesCreated Number of created nodes.
             * @param tries Number of tries to create nodes.
             */
            void updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current& = Ice::Current()) override;

        protected:
            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            CompoundedRequestStrategy() = default;
        };

        /**
         * @brief Implementation of AndBase
         * @see AndBase
         */
        class And:
            virtual public AndBase,
            virtual public CompoundedRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param requestStrategies The strategies forming this compound.
             */
            And(const ComputingPowerRequestStrategyBaseList& requestStrategies):
                AndBase(requestStrategies),
                CompoundedRequestStrategy(requestStrategies)
            {}
            /**
             * @brief Returns true if all compounded strategies return true.
             * @return True if all compounded strategies return true.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override;
        protected:
            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            And() = default;
        };
        typedef IceInternal::Handle<And> AndPtr;

        /**
         * @brief Implementation of OrBase
         * @see OrBase
         */
        class Or:
            virtual public OrBase,
            virtual public CompoundedRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param requestStrategies The strategies forming this compound.
             */
            Or(const ComputingPowerRequestStrategyBaseList& requestStrategies):
                OrBase(requestStrategies),
                CompoundedRequestStrategy(requestStrategies)
            {}
            /**
             * @brief Returns true if any compounded strategies returns true.
             * @return True if all compounded strategies return true.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return std::any_of(requestStrategies.begin(), requestStrategies.end(),
                                   [](ComputingPowerRequestStrategyBasePtr & s)
                {
                    return s->shouldAllocateComputingPower();
                }
                                  );
            }
        protected:
            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            Or() = default;
        };
        typedef IceInternal::Handle<Or> OrPtr;

        /**
         * @brief Implementation of NotBase
         * @see NotBase
         */
        class Not:
            virtual public NotBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param s The negated strategy.
             */
            Not(const ComputingPowerRequestStrategyBasePtr& s): NotBase(s) {}

            /**
             * @brief Passes the call to the contained strategy.
             */
            void setCurrentStateAsInitialState(const ::Ice::Current& = ::Ice::Current()) override
            {
                allocStrat->setCurrentStateAsInitialState();
            }

            /**
             * @brief Passes the call to the contained strategy.
             * @param count The new node count.
             */
            void updateNodeCount(Ice::Long count, const Ice::Current& = Ice::Current()) override
            {
                allocStrat->updateNodeCount(count);
            }

            /**
             * @brief Passes the call to the contained strategy.
             * @param newStatus The new task status.
             */
            void updateTaskStatus(armarx::TaskStatus::Status newStatus, const Ice::Current& = Ice::Current()) override
            {
                allocStrat->updateTaskStatus(newStatus);
            }

            /**
             * @brief Passes the call to the contained strategy.
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override
            {
                allocStrat->allocatedComputingPower();
            }

            /**
             * @brief Returns the negatedresult of the contained strategy.
             * @return The negatedresult of the contained strategy.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return !allocStrat->shouldAllocateComputingPower();
            }

            /**
             * @brief Passes this call to all sub strategies.
             * @see ComputingPowerRequestStrategyBase::updateNodeCreations
             * @param nodesCreated Number of created nodes.
             * @param tries Number of tries to create nodes.
             */
            void updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current& = Ice::Current()) override
            {
                allocStrat->updateNodeCreations(nodesCreated, tries);
            }
        protected:
            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            Not() = default;
        };
        typedef IceInternal::Handle<Not> NotPtr;

        /**
         * @brief Implementation of Always
         * @see Always
         */
        class Always:
            virtual public AlwaysBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Always return true.
             * @return true
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return true;
            }
        };
        typedef IceInternal::Handle<Always> AlwaysPtr;

        /**
         * @brief Implementation of NeverBase
         * @see NeverBase
         */
        class Never:
            virtual public NeverBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Always return false.
             * @return false
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return false;
            }
        };
        typedef IceInternal::Handle<Never> NeverPtr;

        /**
         * @brief Implementation of ElapsedTimeBase
         * @see ElapsedTimeBase
         */
        class ElapsedTime:
            virtual public ElapsedTimeBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief The used clock type.
             */
            typedef std::chrono::system_clock Clock;
            /**
             * @brief The type of the used time point.
             */
            typedef Clock::time_point TimePoint;

            /**
             * @brief Ctor
             * @param timeDeltaInSeconds The delta to use.(in seconds)
             * @param skipping Whether skipping is activated.
             */
            ElapsedTime(Ice::Long timeDeltaInSeconds, bool skipping):
                ElapsedTimeBase(timeDeltaInSeconds, skipping),
                allocatedLastTime {}
            {
            }

            /**
             * @brief Sets the next time to allocate delta seconds from now.
             */
            void setCurrentStateAsInitialState(const ::Ice::Current& = ::Ice::Current()) override
            {
                allocatedLastTime = Clock::now();
            }

            /**
             * @brief Sets the next time to allocate depending on the flag skipping.
             * skipping == true: now + delta seconds.
             * skipping == false: the current next time point + delta seconds. (may already passed)
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Returns true if the last creation timepoint is more than delta seconds away.
             * @return Whether more computing power should be allocated.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return Clock::now() >= allocatedLastTime + std::chrono::seconds {timeDeltaInSeconds};
            }
        protected:
            /**
             * @brief The time point when more power should be allocated again.
             */
            TimePoint allocatedLastTime;

            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            ElapsedTime() = default;
        };
        typedef IceInternal::Handle<ElapsedTime> ElapsedTimePtr;


        /**
         * @brief Implementation of NoNodeCreatedBase
         * @see NoNodeCreatedBase
         */
        class NoNodeCreated:
            virtual public NoNodeCreatedBase,
            virtual public ElapsedTime
        {
        public:
            /**
             * @brief ctor
             * @param timeDeltaInSeconds The delta to use.(in seconds)
             * @param skipping Whether skipping is activated.
             * @param sig The parameter sigma.
             * @param backlogSz The backlog's size.
             */
            NoNodeCreated(Ice::Long timeDeltaInSec, bool skip, float sig, Ice::Long backlogSz):
                NoNodeCreatedBase(timeDeltaInSec, skip, sig, backlogSz),
                ElapsedTime(timeDeltaInSec, skip),
                backlog(backlogSz, 0),
                currentBacklogIndex {0}
            {
                timeDeltaInSeconds = timeDeltaInSec; //somehow does not get set by ctor call
                ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(timeDeltaInSeconds >= 0);
                ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(backlogSize >= 0);
            }

            /**
             * @brief Updates the number of failed node creations.
             * @param nodesCreated Number of created nodes.
             * @param tries Number of tries to create nodes.
             */
            void updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current& = Ice::Current()) override;

            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override;

            /**
             * @brief shouldAllocateComputingPower
             * @return Whrether
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                const auto perc = std::accumulate(backlog.begin(), backlog.end(), 1.f) / backlog.size();
                const auto usedTimeDelta = timeDeltaInSeconds / (1.f + sigma * perc);
                ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(usedTimeDelta >= 0);
                return Clock::now() >= allocatedLastTime + std::chrono::seconds {static_cast<std::size_t>(usedTimeDelta)};
            }

            /**
             * @brief Nulls the backlog after the object was transmitted through ice.
             */
            void ice_postUnmarshal() override
            {
                backlog.assign(backlogSize, 0);
            }

        protected:
            /**
             * @brief The backlog. chars are used instead of bools since vector<bool> is specialized.
             * contains a 1 if a node creation failed and 0 if it was successfull;
             */
            std::vector<char> backlog;
            /**
             * @brief The current index in the backlog. (used to determine which variables will be overwritten)
             */
            std::size_t currentBacklogIndex;

            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            NoNodeCreated() = default;
        };
        typedef IceInternal::Handle<NoNodeCreated> NoNodeCreatedPtr;

        /**
         * @brief Implementation of TotalNodeCountBase
         * @see TotalNodeCountBase
         */
        class TotalNodeCount:
            virtual public TotalNodeCountBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param nodeCountDelta The delta to use.
             * @param skipping Whether skipping is activated.
             */
            TotalNodeCount(Ice::Long nodeCountDelta, bool skipping):
                TotalNodeCountBase(nodeCountDelta, skipping),
                allocateNextTime {0},
                nodeCount {0}
            {
            }

            /**
             * @brief Updates the internal node count.
             * @param newCount The new node count.
             */
            void updateNodeCount(Ice::Long newCount, const Ice::Current& = Ice::Current()) override
            {
                nodeCount = newCount;
            }

            /**
             * @brief Sets the next creation count to allocate depending on the flag skipping.
             * skipping == true: current count + delta.
             * skipping == false: the current next creation count + delta. (may be smaller than the current count)
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Returns true if the the current count is >= the next creation count.
             * @return Whether more computing power should be allocated.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                return nodeCount >= allocateNextTime;
            }

        protected:
            /**
             * @brief The count when to allocate more power.
             */
            Ice::Long allocateNextTime;
            /**
             * @brief The current node count.
             */
            Ice::Long nodeCount;

            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            TotalNodeCount(): allocateNextTime {0}, nodeCount {0} {}
        };
        typedef IceInternal::Handle<TotalNodeCount> TotalNodeCountPtr;

        /**
         * @brief Implementation of TaskStatusBase
         * @see TaskStatusBase
         */
        class TaskStatus:
            virtual public TaskStatusBase,
            virtual public ComputingPowerRequestStrategy
        {
        public:
            /**
             * @brief Ctor
             * @param strategyPerTaskStatus Task status to sub strategy map.
             */
            TaskStatus(TaskStatusMap strategyPerTaskStatus):
                TaskStatusBase(strategyPerTaskStatus),
                current {nullptr}
            {
            }

            /**
             * @brief Passes this call to all sub strategies.
             * @param count The new count
             */
            void updateNodeCount(Ice::Long count, const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Switches the current strategy and passes this call to all sub strategies.
             * @param newStatus
             */
            void updateTaskStatus(armarx::TaskStatus::Status newStatus, const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Passes this call to all sub strategies.
             */
            void allocatedComputingPower(const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Passes this call to all sub strategies.
             * @see ComputingPowerRequestStrategyBase::updateNodeCreations
             * @param nodesCreated Number of created nodes.
             * @param tries Number of tries to create nodes.
             */
            void updateNodeCreations(Ice::Long nodesCreated, Ice::Long tries, const Ice::Current& = Ice::Current()) override;

            /**
             * @brief Returns the result of the current selected strategy.
             * @return The result of the current selected strategy.
             */
            bool shouldAllocateComputingPower(const ::Ice::Current& = ::Ice::Current()) override
            {
                ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(current);
                return current->shouldAllocateComputingPower();
            }
        protected:
            /**
             * @brief The current strategy in use.
             */
            ComputingPowerRequestStrategyBase* current;

            //for object factory
            template<class Base, class Derived> friend class armarx::GenericFactory;
            /**
             * @brief Ctor used for object factories.
             */
            TaskStatus(): current {nullptr} {}
        };
        typedef IceInternal::Handle<TaskStatus> TaskStatusPtr;
    }
}
