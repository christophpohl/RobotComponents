#pragma once

class GazeStabOptions
{
public:
    GazeStabOptions(int model);
    ~GazeStabOptions();

    int model; // robot model used (0 = armar 4 WO torso; 1 = armar 4 W torso)

    int pseudo_inverse;     // use the Moore–Penrose pseudoinverse (min qdhead^2 + qdvirt)
    int weighted_inverse;   // use the weighted pseudoinverse (min qdhead + lambda*OptFl)

    double lambda; // balance gain in weighted inverse minimization
};

