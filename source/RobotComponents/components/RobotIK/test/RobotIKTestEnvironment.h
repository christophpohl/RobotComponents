/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents::ArmarXObjects::RobotIKTestEnvironment
* @author     Joshua Haustein ( joshua dot haustein at gmail dot com)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <RobotComponents/components/RobotIK/RobotIK.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>

#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <VirtualRobot/XML/RobotIO.h>

#include <RobotComponents/Test.h>

using namespace armarx;

class RobotIKTestEnvironment
{
public:
    RobotIKTestEnvironment(const std::string& testName, bool loadSpaces = false, int registryPort = 11220, bool addObjects = true)
    {
        Ice::PropertiesPtr properties = Ice::createProperties();
        armarx::Application::LoadDefaultConfig(properties);
        const std::string project = "RobotAPI";
        armarx::CMakePackageFinder finder(project);

        if (!finder.packageFound())
        {
            ARMARX_ERROR_S << "ArmarX Package " << project << " has not been found!";
        }
        else
        {
            armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
        }

        std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
        ArmarXDataPath::getAbsolutePath(fn, fn);
        std::string robotFilename = fn;//test::getCmakeValue("PROJECT_DATA_DIR") + "/RobotAPI/tests/robotmodel/ArmarIII.xml";
        properties->setProperty("ArmarX.RobotStateComponent.RobotFileName", robotFilename);
        properties->setProperty("ArmarX.RobotStateComponent.ObjectName", "RobotStateComponent");
        properties->setProperty("ArmarX.RobotStateComponent.AgentName", "Armar3");
        properties->setProperty("ArmarX.RobotStateComponent.RobotNodeSetName", "Robot");
        properties->setProperty("ArmarX.RobotIK.RobotFileName", robotFilename);
        properties->setProperty("ArmarX.RobotIK.RobotStateComponentName", "RobotStateComponent");

        _spaceSavePath = test::getCmakeValue("PROJECT_BINARY_DIR") + "/Testing/Temporary/spaces/";

        if (loadSpaces)
        {
            properties->setProperty("ArmarX.RobotIK.ReachabilitySpacesFolder", _spaceSavePath);
            properties->setProperty("ArmarX.RobotIK.InitialReachabilitySpaces", "testReachabilitySpace.bin");
        }

        _iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
        _iceTestHelper->startEnvironment();

        _manager = new TestArmarXManager(testName, _iceTestHelper->getCommunicator(), properties);

        if (addObjects)
        {
            _robotStateComponent = _manager->createComponentAndRun<RobotStateComponent, RobotStateComponentInterfacePrx>("ArmarX", "RobotStateComponent");
            _robotIK = _manager->createComponentAndRun<RobotIK, RobotIKInterfacePrx>("ArmarX", "RobotIK");
        }

        _robotModel = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eStructure);
    }

    ~RobotIKTestEnvironment()
    {
        _manager->shutdown();
    }

    RobotStateComponentInterfacePrx _robotStateComponent;
    RobotIKInterfacePrx _robotIK;
    TestArmarXManagerPtr _manager;
    IceTestHelperPtr _iceTestHelper;
    VirtualRobot::RobotPtr _robotModel;
    std::string _spaceSavePath;
};

typedef boost::shared_ptr<RobotIKTestEnvironment> RobotIKTestEnvironmentPtr;

