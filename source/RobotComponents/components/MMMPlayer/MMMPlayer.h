/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::MMMPlayer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Model/Model.h>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/RobotPoseUnitInterface.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <RobotAPI/libraries/core/PIDController.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotComponents/interface/components/MMMPlayerInterface.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <RobotAPI/libraries/core/Trajectory.h>

//#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>


namespace armarx
{
    /**
             * \class MMMPlayerPropertyDefinitions
             * \brief
             */
    class MMMPlayerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        MMMPlayerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ArmarXProjects", "", "Comma-separated list with names of ArmarXProjects (e.g. 'Armar3,Armar4'). The MMM XML File can be specified relatively to a data path of one of these projects.");
            defineOptionalProperty<std::string>("MMMFile", "", "Path to MMM XML File");
            defineOptionalProperty<std::string>("ModelFile", "", "Path to Model XML File");
            defineOptionalProperty<bool>("ApplyButterworthFilter", false, "If true a butterworth filter is applied on the trajectory.");
            defineOptionalProperty<float>("ButterworthFilterCutOffFreq", 20, "Cut off frequency for the butterworth lowpass filter.");


            //            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the KinematicUnit to which the joint values should be sent");
            //            defineRequiredProperty<std::string>("KinematicTopicName", "Name of the KinematicUnit reporting topic");
            //
            //            defineOptionalProperty<float>("FPS", 100.0f, "FPS with which the recording should be executed. Velocities will be adapted.");
            //            defineOptionalProperty<bool>("LoopPlayback", false, "Specify whether motion should be repeated after execution");
            //            defineOptionalProperty<bool>("UsePIDController", true, "Specify whether the PID Controller should be used (==velocity control)");
            //            defineOptionalProperty<float>("Kp", 1.f, "Proportional gain value of PID Controller");
            //            defineOptionalProperty<float>("Ki", 0.00001f, "Integral gain value of PID Controller");
            //            defineOptionalProperty<float>("Kd", 1.0f, "Differential gain value of PID Controller");
            //            defineOptionalProperty<float>("absMaximumVelocity", 120.0f, "The PID will never set a velocity above this threshold (degree/s)");
            //            defineOptionalProperty<std::string>("RobotNodeSetName", "RobotState", "The name of the robot node set to be used (only need for PIDController mode)");

            //            defineOptionalProperty<bool>("EnableRobotPoseUnit", false, "Specify whether RobotPoseUnit should be used to move the robot's position. Only useful in simulation.");
            //            defineOptionalProperty<std::string>("RobotPoseUnitName", "RobotPoseUnit", "Name of the RobotPoseUnit to which the robot position should be sent");
            //            defineOptionalProperty<float>("Offset.x", 0.f, "x component of initial robot position (mm)");
            //            defineOptionalProperty<float>("Offset.y", 0.f, "y component of initial robot position (mm)");
            //            defineOptionalProperty<float>("Offset.z", 0.f, "z component of initial robot position (mm)");
            //            defineOptionalProperty<float>("Offset.roll", 0.f, "Initial robot pose: roll component of RPY angles (radian)");
            //            defineOptionalProperty<float>("Offset.pitch", 0.f, "Initial robot pose: pitch component of RPY angles (radian)");
            //            defineOptionalProperty<float>("Offset.yaw", 0.f, "Initial robot pose: yaw component of RPY angles (radian)");
            //            defineOptionalProperty<float>("Offset.yaw", 0.f, "Initial robot pose: yaw component of RPY angles (radian)");

        }
    };

    /**
             * \defgroup Component-MMMPlayer MMMPlayer
             * \ingroup RobotComponents-Components
             * \brief Replays an MMM trajectory from a file.
             *
             * MMMPlayer reads an MMM trajectory from an MMM XML file (component property) and replays the motion using the KinematicUnit and its currently loaded robot.
             * The trajectory can be replayed using position control or velocity control.
             * In the latter case, the control parameters (P, I, D) can be configured via component properties.
             */

    /**
             * @ingroup Component-MMMPlayer
             * @brief The MMMPlayer class
             */
    class MMMPlayer :
        virtual public armarx::Component,
        public armarx::MMMPlayerInterface
    {
    public:
        /**
                 * @see armarx::ManagedIceObject::getDefaultName()
                 */
        std::string getDefaultName() const override
        {
            return "MMMPlayer";
        }

    protected:
        /**
                 * @see armarx::ManagedIceObject::onInitComponent()
                 */
        void onInitComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onConnectComponent()
                 */
        void onConnectComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onDisconnectComponent()
                 */
        void onDisconnectComponent() override;

        /**
                 * @see armarx::ManagedIceObject::onExitComponent()
                 */
        void onExitComponent() override;

        /**
                 * @see PropertyUser::createPropertyDefinitions()
                 */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        MMM::LegacyMotionPtr motion;

        Ice::StringSeq jointNames;

        std::string motionPath;
        std::string modelPath;
    private:
        void load(const std::string& filename, const std::string& projects);

        RecursiveMutex mmmMutex;
    public:
        // MMMPlayerInterface
        bool loadMMMFile(const std::string& filename, const std::string& projects, const Ice::Current&) override;
        int getNumberOfFrames(const Ice::Current&) override;
        std::string getMotionPath(const Ice::Current&) override;
        std::string getModelPath(const Ice::Current&) override;
        Ice::StringSeq getJointNames(const Ice::Current&) override;

        //        virtual TrajSource getTrajectory(const Ice::Current&);

        bool isMotionLoaded(const Ice::Current&) override
        {
            if (!motion)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        TrajectoryBasePtr getJointTraj(const Ice::Current&) override;
        TrajectoryBasePtr getBasePoseTraj(const Ice::Current&) override;

    };

    typedef ::IceInternal::Handle< ::armarx::MMMPlayer> MMMPlayerPtr;

}

