/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::GraspingManager
 * @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/GraspingManager/GraspingManagerInterface.h>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.h>
#include <RobotComponents/interface/components/GraspingManager/GraspSelectionManagerInterface.h>
#include <RobotComponents/interface/components/GraspingManager/RobotPlacementInterface.h>
#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/interface/components/RobotIK.h>
#include <RobotComponents/interface/components/PlannedMotionProviderInterface.h>
#include <RobotComponents/components/MotionPlanning/Tasks/CSpaceVisualizerTask/CSpaceVisualizerTask.h>

#include <RobotAPI/interface/core/RobotState.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/gui/EntityDrawerInterface.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <Eigen/Geometry>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>

namespace armarx
{
    /**
     * @class GraspingManagerPropertyDefinitions
     * @brief
     */
    class GraspingManagerPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        GraspingManagerPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("GraspGeneratorName", "SimpleGraspGenerator", "Name of the GraspGenerator proxy");
            defineOptionalProperty<std::string>("RobotPlacementName", "SimpleRobotPlacement", "Name of the RobotPlacement proxy");
            defineOptionalProperty<std::string>("RobotNodeSetNames", "HipYawRightArm;HipYawLeftArm", "Names of the robot node sets to use for IK calculations (';' delimited)");
            defineOptionalProperty<std::string>("RobotCollisionNodeSet", "Robot", "Name of the collision nodeset used for motion planning", PropertyDefinitionBase::eModifiable);
            defineRequiredProperty<std::string>("JointToLinkSetMapping", "Mapping from joint set to link set, i.e. which collision set to use for which kinematic chain. Format: JointSet1:CollisionSet1;JointSet2:CollisionSet2", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<std::string>("ReachabilitySpaceFilePaths",
                                                "Armar3/reachability/reachability_right_hand_smoothened.bin;Armar3/reachability/reachability_left_hand_smoothened.bin",
                                                "Paths to the reachability space files (needed only if no reachability space is loaded for the chosen RobotNodeSets) (';' delimited)");
            defineOptionalProperty<std::string>("PlanningBoundingBox", "-15000, -4000, -3.1416, 15000, 15000, 3.1416", "x_min, y_min, alpha_min, x_max, y_max, alpha_max");
            defineOptionalProperty<float>("VisualizationSlowdownFactor", 1.0f, "1.0 is a good value for clear visualization, 0 the visualization should not slow down the process", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("MaxDistanceForDirectGrasp", 1200.0f, "x-y-Distance from robot base to object for which the grasping manager tries to find a solution without platform movement", PropertyDefinitionBase::eModifiable);



        }
    };

    /**
     * @defgroup Component-GraspingManager GraspingManager
     * @ingroup RobotComponents-Components
     * @brief Provides methods for grasp planning purposes.
     *
     * The GraspingManager implements a grasping pipeline containing the following stages (each stage is implemented by the respective Component in the parathesis):
     * \li Grasp generation (SimpleGraspGenerator)
     * \li Grasp filtering (GraspSlectionManager)
     * \li Robot placement (SimpleRobotPlacement)
     * \li Finding a target configuration (RobotIK)
     * \li Trajectory planning (MotionPlanningServer)
     *
     * The stages are executed sequentially.
     * The Input for the grasping pipeline is a graspable object.
     * The output contains trajectories in configuration and pose space to execute a grasp for the given object.
     *
     * The GraspingManager requires the following properties:
     * \li a list of robotnodeset names (; delimited) for IK calculations
     * \li a list of workspace files (; delimited) for reachability considerations
     *
     * \note The Armar3Simulation scenario must be active before running the GraspingManager scenario.
     */

    /**
     * @ingroup Component-GraspingManager
     * @brief The GraspingManager class
     */
    class GraspingManager :
        virtual public Component,
        virtual public GraspingManagerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "GraspingManager";
        }

        /**
         * @brief runs the grasping pipeline
         * @param objectInstanceEntityId the id for retrieval of the target object from working memory.
         * @param c
         * @return A list of grasping trajectories in configuration and pose space.
         */
        GraspingTrajectory generateGraspingTrajectory(const std::string& objectInstanceEntityId, const Ice::Current& c = Ice::Current()) override;


    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new GraspingManagerPropertyDefinitions(getConfigIdentifier()));
        }


        std::pair<std::string, Ice::StringSeq> visualizeGrasp(const GeneratedGrasp& grasp, int id, const DrawColor& color = DrawColor {1, 1, 1, 1});
    private:
        SimoxCSpacePtr createCSpace();
        GeneratedGraspList generateGrasps(const std::string& objectInstanceEntityId);
        GeneratedGraspList filterGrasps(const GeneratedGraspList& grasps);
        GraspingPlacementList generateRobotPlacements(const GeneratedGraspList& grasps, const std::string& objectInstanceEntityId);
        GraspingTrajectory planMotion(const MotionPlanningData& mpd);
        void drawTrajectory(const GraspingTrajectory& t);
        std::vector<MotionPlanningData> calculateIKs(const GraspingPlacementList& graspPlacements);
        NameValueMap calculateSingleIK(const ::std::string& robotNodeSetName, const std::string& eef, const PoseBasePtr& globalRobotPose, const ::armarx::FramedPoseBasePtr& tcpPose, bool checkCollisionFree = true);

        armarx::StringStringDictionary getJointSetCollisionSetMapping();

        void setNextStepDescription(const std::string& description, const std::string& objId);
        void resetStepDescription();
        std::string graspGeneratorName;
        std::string robotPlacementName;
        std::vector<std::string> robotNodeSetNames;
        std::vector<std::string> reachabilitySpaceFilePaths;

        Vector3fRange planningBoundingBox;
        GraspGeneratorInterfacePrx gg;
        GraspSelectionManagerInterfacePrx gsm;
        RobotPlacementInterfacePrx rp;

        PlannedMotionProviderInterfacePrx pmp;
        SimoxCSpacePtr cacheCSpace;

        RobotIKInterfacePrx rik;
        RobotStateComponentInterfacePrx rsc;
        VirtualRobot::RobotPtr localRobot, ikRobot;
        SimoxCSpacePtr cspace;

        memoryx::CommonStorageInterfacePrx cs;
        memoryx::WorkingMemoryInterfacePrx wm;
        memoryx::PriorKnowledgeInterfacePrx prior;

        memoryx::EntityDrawerInterfacePrx entityDrawer;
        std::string layerName;
        std::string robotVisuId;

        std::string stepDescription;
        int step = 0;

        std::vector<CSpaceVisualizerTaskHandle> planningTasks;
        Mutex graspManagerMutex;
    };
}

