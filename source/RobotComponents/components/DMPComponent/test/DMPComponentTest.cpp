/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::DMPComponent
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotComponents::ArmarXObjects::DMPComponent

#define ARMARX_BOOST_TEST

#include <RobotComponents/Test.h>
#include <RobotComponents/components/DMPComponent/DMPComponent.h>
#include <RobotComponents/interface/components/DMPComponentBase.h>

#include "DMPComponentEnvironment.h"


#include <iostream>

//::armarx::cStateVec getcStateVec(const DMP::Vec<DMP::DMPState> &dmpstate){
//    ::armarx::cStateVec sv;
//    sv.resize(dmpstate.size());
//    for(size_t i = 0; i < dmpstate.size(); i++){
//        sv[i].pos = dmpstate[i].pos;
//        sv[i].vel = dmpstate[i].vel;
//    }
//    return sv;
//}

//::armarx::cStateVec getcStateVec(const DMP::Vec<DMP::_3rdOrderDMP> &dmpstate){
//    ::armarx::cStateVec sv;
//    sv.resize(dmpstate.size());
//    for(size_t i = 0; i < dmpstate.size(); i++){
//        sv[i].pos = dmpstate[i].pos;
//        sv[i].vel = dmpstate[i].vel;
//        sv[i].acc = dmpstate[i].acc;
//    }
//    return sv;
//}

//bool addedDataDir = false;

//BOOST_AUTO_TEST_CASE(test1)
//{
//    armarx::DMPComponent instance;
//    instance.instantiateDMP(ARMARX_DMPTYPE_BASICDMP);

//    armarx::configMap configs;
//    configs.insert(armarx::configPair(DMP_PARAMETERS_SPRINGCONSTANT,1.1));

//    if (!addedDataDir) {
//        armarx::ArmarXDataPath::addDataPaths(armarx::test::getCmakeValue("PROJECT_DATA_DIR"));
//        addedDataDir = true;
//    }

//    instance.setConfigurationMap(configs);
//    std::string absDMPPath;
//    armarx::ArmarXDataPath::getAbsolutePath("dmp/sampletraj.csv", absDMPPath);
//    instance.readTrajectoryFromFile(absDMPPath);
//    instance.trainDMP();
//    std::cout << instance.getDMPTypeName() << std::endl;

//    BOOST_CHECK_EQUAL(true, true);

//}

BOOST_AUTO_TEST_CASE(DMPSerialization)
{
    DMPComponentEnvironmentPtr env(new DMPComponentEnvironment("DMPtestenv"));
    std::string filename = "/home/zhou/aaa.xml";
    env->dmp->getDMPFromFile(filename, "myDMP");

}

BOOST_AUTO_TEST_CASE(storeAndremoveDMPTest)
{
    //

    //    env->dmp->usingBasicDMP();
    //    env->dmp->instantiateDMP(ARMARX_DMPTYPE_BASICDMP);
    //    env->dmp->readTrajectoryFromFile("/home/yzhou/motions/sampletraj.csv");
    //    env->dmp->trainDMP();

    //    std::string name = "theThirdDMP";

    //    env->dmp->storeDMPInDatabase(name);
    //    env->dmp->removeDMPFromDatabase(name);
}

BOOST_AUTO_TEST_CASE(retrieveDMPTest)
{
    //    DMPComponentEnvironmentPtr env(new DMPComponentEnvironment("DMPtestenv"));

    //    std::string dmpname= "theThirdDMP";
    //    env->dmp->getDMPFromDatabase(dmpname);

    //    DMP::SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("/home/yzhou/motions/sampletraj.csv");
    //    double pos = traj.begin()->getPosition(0);
    //    std::vector<double> startpos;
    //    startpos.push_back(pos);
    //    double goal = traj.rbegin()->getPosition(0);
    //    std::vector<double> goals;
    //    goals.push_back(goal);
    //    std::vector<double> timestamps = DMP::SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));


    //    //setting dmp parameters
    //    env->dmp->setGoal(goals);
    //    env->dmp->setStartPosition(startpos);
    //    armarx::cStateVec currentState;
    //    armarx::cState onestate;
    //    onestate.pos = pos;
    //    onestate.vel = 0;
    //    onestate.acc = 0;
    //    currentState.push_back(onestate);


    //    env->dmp->setParameter(DMP_PARAMETERS_TEMPORALFACTOR,1.0);
    //    env->dmp->setTimeStamps(timestamps);
    //    env->dmp->setDMPState(currentState);

    //    std::vector<double> canVals;
    //    canVals.push_back(1.0);
    //    env->dmp->setCanonicalValues(canVals);
    //    for(int i = 0; i < timestamps.size(); i++){
    //        currentState = env->dmp->getNextState();

    //        env->dmp->setDMPState(currentState);
    //        std::cout << "pos: " << currentState[0].pos << std::endl;
    //    }

}

BOOST_AUTO_TEST_CASE(deleteDMPTest)
{
    //    DMPComponentEnvironmentPtr env(new DMPComponentEnvironment("DMPtestenv"));
    //    std::string dmpname = "theThirdDMP";
    //    env->dmp->removeDMPFromDatabase(dmpname);
}
