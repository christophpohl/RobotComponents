armarx_component_set_name("FunctionApproximator")

find_package(DMP QUIET)
armarx_build_if(DMP_FOUND "DMP not available")

find_package(MMMCore QUIET)
armarx_build_if(MMMCore_FOUND "MMMCore not available")

find_package(MMMTools QUIET)
armarx_build_if(MMMTools_FOUND "MMMTOOLS not available")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

find_package(IVT QUIET)
armarx_build_if(IVT_FOUND "IVT not available")


if(DMP_FOUND AND MMMCore_FOUND AND Eigen3_FOUND AND MMMTools_FOUND)
    include_directories(${DMP_INCLUDE_DIRS})
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
    link_directories(${IVT_LIBRARY_DIRS})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ${DMP_LIBRARIES} MMMCore RobotComponentsInterfaces)

set(SOURCES FunctionApproximator.cpp)
set(HEADERS FunctionApproximator.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
target_compile_options(FunctionApproximator PUBLIC "-fopenmp")

# add unit tests
add_subdirectory(test)
